package com.usw.tokenframecontroldemo.HwInterfaceManager;

import android.content.Context;
import android.os.Build;
import android.util.Log;

public class HwInterfaceManager {

    private static final String TAG = HwInterfaceManager.class.getSimpleName();

    private static HwInterfaceManager mInstance = null;
    private Context                   mContext;
    private String                    boardType;

    //  Board Type definition
    public static final String BOARD_TYPE_BND           =   "BND";
    public static final String BOARD_TYPE_SMDT          =   "SMDT-40X";
    public static final String BOARD_TYPE_SMDT_3399X    =   "SMDT-3399X";
    public static final String BOARD_TYPE_YF3568        =   "YF3568";
    public static final String BOARD_TYPE_A133          =   "A133";

    /**
     *
     */
    private HwInterfaceManager()
    {

    }

    /***
     *
     * @param context
     */
    private HwInterfaceManager(Context context)
    {
        this.mContext = context;
    }

    /**
     *
     * @return
     */
    public static HwInterfaceManager getInstance()
    {
        return  mInstance;
    }

    /**
     *
     * @param context
     * @return
     */
    public static HwInterfaceManager getAndSetupInstance(Context context)
    {
        if  (mInstance == null)
        {
            synchronized (HwInterfaceManager.class) {

                mInstance = new HwInterfaceManager(context);

                //  Load Configuration by BuildConfig and Setup Context
                mInstance.AutoLoadConfiguration();
            }
        }

        return  mInstance;
    }

    /**
     *
     * @return
     */
    public boolean AutoLoadConfiguration()
    {
        String  deviceModel;
        String  deviceProduct;

        deviceModel     =   getDeviceModel();
        deviceProduct   =   getDeviceProduct();

        Log.i(TAG, "deviceModel = " + deviceModel + ",deviceProduct = " + deviceProduct);

        if  (deviceModel.contains("TF-2108") || deviceProduct.contains("TF-2108"))
        {
            //  RK312x-TF2108 Board
            this.setBoardType(BOARD_TYPE_BND);
        }
        else if  (deviceModel.contains("a40") || deviceModel.contains("A40"))
        {
            Log.i(TAG, "SMDT 40X Board");

            //  SMDT 40X Board
            this.setBoardType(BOARD_TYPE_SMDT);
        }
        else if (deviceModel.contains("rk3399_all") || deviceProduct.contains("rk3399_all"))
        {
            Log.i(TAG, "SMDT 3399X Board");

            //  rk3399_all Board
            this.setBoardType(BOARD_TYPE_SMDT_3399X);
        }
        else if (deviceModel.contains("CPF-01"))
        {
            Log.i(TAG, "A133 Board");

            //  A133 Board
            this.setBoardType(BOARD_TYPE_A133);
        }
        else if (deviceModel.contains("YF_020E") || deviceModel.contains("YXW_020E"))
        {
            Log.i(TAG, "YoungFeel YF_020E Board");

            //  YF3568 Board
            this.setBoardType(BOARD_TYPE_YF3568);
        }
        else
        {
            Log.i(TAG, "Default to SMDT 40X Board");

            //  Default to SMDT Board
            this.setBoardType(BOARD_TYPE_SMDT);
        }

        return  true;
    }

    /**
     *
     * @return
     */
    public String getDeviceModel() {

        return  Build.MODEL;
    }

    /**
     *
     * @return
     */
    public String getDeviceProduct() {

        return  Build.PRODUCT;
    }

    /**
     *
     * @return
     */
    public String getDeviceInfo() {

        String phoneInfo = "Product: " + Build.PRODUCT+"\n";
        phoneInfo += ", CPU_ABI: " + Build.CPU_ABI+"\n";
        phoneInfo += ", TAGS: " + Build.TAGS+"\n";
        phoneInfo += ", VERSION_CODES.BASE: " + Build.VERSION_CODES.BASE+"\n";
        phoneInfo += ", MODEL: " + Build.MODEL+"\n";
        phoneInfo += ", SDK: " + Build.VERSION.SDK+"\n";
        phoneInfo += ", VERSION.RELEASE: " + Build.VERSION.RELEASE+"\n";
        phoneInfo += ", DEVICE: " + Build.DEVICE+"\n";
        phoneInfo += ", DISPLAY: " + Build.DISPLAY+"\n";
        phoneInfo += ", BRAND: " + Build.BRAND+"\n";
        phoneInfo += ", BOARD: " + Build.BOARD+"\n";
        phoneInfo += ", FINGERPRINT: " + Build.FINGERPRINT+"\n";
        phoneInfo += ", ID: " + Build.ID+"\n";
        phoneInfo += ", MANUFACTURER: " + Build.MANUFACTURER+"\n";
        phoneInfo += ", USER: " + Build.USER+"\n";

        return   phoneInfo;
    }

    //-----------------------------------------------
    //
    //-----------------------------------------------

    public void setupContext(Context context) {

        this.mContext = context;
    }

    public String getBoardType() {
        return boardType;
    }

    public void setBoardType(String boardType) {
        this.boardType = boardType;
    }
}
