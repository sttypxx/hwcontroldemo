package com.usw.tokenframecontroldemo.service;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

//import com.orhanobut.logger.Logger;
//import com.usw.adplayer.Activity.LauncherActivity;
//import com.usw.adplayer.HwInterfaceManager.HwInterfaceManager;
//import com.usw.adplayer.utils.AppUtils;
//import com.usw.adplayer.utils.ShellUtils;

import com.usw.tokenframecontroldemo.AppUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 */
public class GuardService extends Service {

    private String guardAppName             =   "com.using.guardapp";
    private String guardAppMainActivityName =   "com.using.guardapp.MainActivity";

    private static final String FrameWorkAppName = "com.usw.tokenframehwcontrol";
    private static final String TinyDemoAppName = "com.usw.tinydemo";

    private GuardThread guardThread;

    /**
     *
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     *
     */
    @Override
    public void onCreate() {

        super.onCreate();

        Log.e("TAG", "后台保活Service启动");

        guardThread = new GuardThread();
        guardThread.setRun(true);
        guardThread.start();
    }

    /**
     *
     */
    @Override
    public void onDestroy() {

        super.onDestroy();

        if  (guardThread != null) {

            guardThread.setRun(false);
            guardThread = null;
        }
    }

    /**
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return  START_STICKY;
    }

    /**
     *
     */
    private class GuardThread extends Thread {

        private boolean isRun;

        public void setRun(boolean run) {

            isRun = run;
        }

        @Override
        public void run() {

            while (isRun) {

//                if  (!AppUtils.isForeground(getApplicationContext(), LauncherActivity.class.getName())) {
//
//                    Log.e("TAG", "广告页面不在前台，重启App");
//                    AppUtils.restartApp(getApplicationContext(), LauncherActivity.class);
//                }

                int uid = AppUtils.getPackageUid(getApplicationContext(), TinyDemoAppName);
                if  (uid > 0) {

                    boolean rstA = AppUtils.isAppRunning(getApplicationContext(), TinyDemoAppName);
                    boolean rstB = AppUtils.isProcessRunning(getApplicationContext(), uid);

                    if  (rstA || rstB) {

                        //  指定包名的程序正在运行中
                        Log.e("TAG", "守护app运行中");
                    } else {

                        //  指定包名的程序未在运行中
                        Log.e("TAG", "守护app没有启动，重新启动");

                        try
                        {
                            //  Start Not Hidden App
                            AppUtils.doStartApplicationWithPackageName(getApplicationContext(), TinyDemoAppName);
//                            AppUtils.startPackageWithBroadcast(getApplicationContext());
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }

                        //  Start Hide App
//                        AppUtils.doStartHideAppWithPackageName(getApplicationContext(), guardAppName, guardAppMainActivityName);
                    }
                } else {

                    //  应用未安装
                    Log.e("TAG", "守护app没有安装，安装app");

//                    try
//                    {
//                        InputStream is = getAssets().open("guard.apk");
//                        String path = inputstreamtofile(is).getAbsolutePath();
//
//                        Logger.e("开始执行静默安装守护apk");
//                        HwInterfaceManager.getInstance().silentInstall(path);
//
////                        String cmd = "pm install -rf " + path;
////                        if  (ShellUtils.checkRootPermission()) {
////
////                            ShellUtils.CommandResult commandResult = ShellUtils.execCommand(cmd, true);
////                            Logger.e(commandResult.errorMsg);
////                            Logger.e(commandResult.successMsg);
////                            Logger.e("开始执行静默安装守护apk");
////                        }
//
//                    } catch (IOException e) {
//
//                        e.printStackTrace();
//                    }
                }

                try
                {
                    Thread.sleep(5 * 1000);

                } catch (InterruptedException e) {

                    e.printStackTrace();
                }
            }
        }

        /**
         *
         * @param ins
         * @return
         * @throws IOException
         */
        private File inputstreamtofile(InputStream ins) throws IOException {

            File file = File.createTempFile("guard", ".apk", new File(Environment.getExternalStorageDirectory().getPath()));
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {

                os.write(buffer, 0, bytesRead);
            }

            os.close();
            ins.close();

            return file;
        }
    }
}

