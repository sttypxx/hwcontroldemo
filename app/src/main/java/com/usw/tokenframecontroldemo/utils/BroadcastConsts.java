package com.usw.tokenframecontroldemo.utils;

public class BroadcastConsts {

    public static final String MESSAGE               =  "message";
    public static final String POWERENABLE_STRING    =  "powerable_string";

    public static final String ACTION_BOOT           =  "android.intent.action.BOOT_COMPLETED";
    public static final String ACTION_REQUEST_BOOT   =  "com.usw.request_boot";

    //  Install Apk and Uninstall
    public static final String ACTION_INSTALL_APK    =  "com.usw.silentinstall";
    public static final String APK_FULL_PATH         =  "apk_full_path";

    public static final String ACTION_SILIENT_UNINSTALL = "com.usw.uninstallsilent";
    public static final String APP_NAME              =  "app_name";

    //  Reboot and Shutdown
    public static final String ACTION_REBOOT         =  "com.usw.system.reboot";
    public static final String ACTION_SHUTDOWN       =  "com.usw.system.shutdown";

    //  Screen On and Off
    public static final String ACTION_SCREEN_ON      =  "com.usw.system.screen_on";
    public static final String ACTION_SCREEN_OFF     =  "com.usw.system.screen_off";

    //  Set Brightness
    public static final String ACTION_BRIGHTNESS_CONTROL = "com.usw.brightness.control";
    public static final String KEY_BRIGHTNESS        =  "brightness";

    //  Set BkLight
    public static final String ACTION_BACKLIGHT_CONTROL =   "com.usw.backlight.control";
    public static final String KEY_BACKLIGHT_ENABLE  =  "BackLightEnable";

    //  Set Time
    public static final String ACTION_SET_TIME       =  "com.usw.set.system.set_time";
    public static final String TIME_ARRAY            =  "currentTime";

    //  Set up Timing Switch
    public static final String ACTION_TIMING_SWITCH  =  "com.usw.set.time_switch";
    public static final String POWER_STRING          =  "power_string";

    //  Delete Timing Switch
    public static final String ACTION_TIME_DELETE    =  "com.usw.set.time_delete";

    //  Take Screen shot
    public static final String ACTION_SCREENSHOT     =  "com.usw.takescreen";
    public static final String PARAM_FULL_PATH       =  "full_path";
    public static final String PARAM_FILE_NAME       =  "fileName";

    //  Action Connect Wifi
    public static final String ACTION_CONNECT_WIFI   =  "com.usw.connect_wifi";
    public static final String KEY_SSID              =  "ssid"; //  ssid
    public static final String KEY_PASSWORD          =  "pass"; //  password, public network if empty
    public static final String KEY_TYPE              =  "type"; //  type, encryption type

    //------------------------------------------------------------------
    //  Broadcasts sent back
    //  1.  Report WiFi Connection Status
    public static final String ACTION_WIFI_CONNECT_STATUS   =   "com.usw.connect_wifi.status";
    public static final String STATUS_KEY_SSID          =   "ssid";
    public static final String STATUS_KEY_RESULT        =   "result";

    //  2.  Report data Received from Android Phone via BLE
    public static final String ACTION_DATA_AVAILABLE    =   "com.usw.bluetooth.ACTION_DATA_AVAILABLE";
    public static final String EXTRA_DATA               =   "com.usw.bluetooth.EXTRA_DATA";

    public static final String action_startup_player    =   "qihang.intent.action.STARTUP_PLAYER";
    public static final String ACTION_TIMEONLINE        =   "qihang.intent.action.TIMEONLINE";
    public static final String ACTION_TOAST             =   "qihang.intent.action.TOAST";

    //  Set Time Index
    public static final int     YEAR_INDEX      =   0;
    public static final int     MONTH_INDEX     =   1;
    public static final int     DAY_INDEX       =   2;
    public static final int     HOUR_INDEX      =   3;
    public static final int     MINUTE_INDEX    =   4;
    public static final int     SECOND_INDEX    =   5;
}
