package com.usw.tokenframecontroldemo.utils;

import java.util.Random;

public class RandomUtil {

    /**
     * 生成随机串(包含 字母+数字)
     * @param pwd_len // 随机数的长度
     * @return 字符串
     */
    public static String genRandomNum(int pwd_len) {

        final int maxNum = 50;
        int i;
        int count = 0;
        char[] str = {'a', 'A', 'b', 'B', 'c', 'C', 'd', 'D', 'e', 'E', 'f', 'F',
                      'g', 'G', 'h', 'H', 'i', 'I', 'j', 'J', 'k', 'K', 'l', 'm',
                      'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'X',
                      'y', 'Y', 'z', 'Z', '0', '1', '2', '3', '4', '5', '6', '7',
                      '8', '9'};

        StringBuffer pwd = new StringBuffer("");
        Random r = new Random();
        while (count < pwd_len) {

            i = Math.abs(r.nextInt(maxNum));

            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }

        return  pwd.toString();
    }
}
