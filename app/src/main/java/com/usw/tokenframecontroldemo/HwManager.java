package com.usw.tokenframecontroldemo;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.usw.tokenframecontroldemo.bean.DutyTime;
import com.usw.tokenframecontroldemo.utils.BroadcastConsts;

import java.util.ArrayList;

public class HwManager {

    private static final String TAG = HwManager.class.getSimpleName();

    private static HwManager sInstance = null;
    private Context mContext;

    private HwManager(Context context) {

        this.mContext = context;
    }

    public static HwManager getInstance(Context context) {

        synchronized (HwManager.class)
        {
            if  (null == sInstance) {

                sInstance = new HwManager(context);
            }

            return  sInstance;
        }
    }

    /**
     *  Reboot
     */
    public void reboot()
    {
        Log.e(TAG, "send com.usw.system.reboot broadcast");

        mContext.sendBroadcast(new Intent(BroadcastConsts.ACTION_REBOOT));
    }

    /**
     * Shutdown
     */
    public void shutdown()
    {
        mContext.sendBroadcast(new Intent(BroadcastConsts.ACTION_SHUTDOWN));
    }

    /**
     *  Take Screen Shot
     *  @param fullPath
     *  @param fileName
     *  @return
     */
    public boolean takeScreenshot(String fullPath, String fileName)
    {
        Intent intent = new Intent();

        intent.setAction(BroadcastConsts.ACTION_SCREENSHOT);
        
        intent.putExtra(BroadcastConsts.PARAM_FULL_PATH, fullPath);
        intent.putExtra(BroadcastConsts.PARAM_FILE_NAME, fileName);

        mContext.sendBroadcast(intent);

        return  true;
    }

    /**
     *  Turn on Screen
     *  @param paramContext
     */
    public void screenOn(Context paramContext)
    {
        Log.e(TAG, "send com.usw.system.screen_on broadcast");

        Intent  intent = new Intent(BroadcastConsts.ACTION_SCREEN_ON);
        mContext.sendBroadcast(intent);
    }

    /**
     *  Turn off Screen
     *  @param paramContext
     */
    public void screenOff(Context paramContext)
    {
        Log.e(TAG, "send com.usw.system.screen_off broadcast");

        Intent  intent = new Intent(BroadcastConsts.ACTION_SCREEN_OFF);
        mContext.sendBroadcast(intent);
    }

    /**
     *     From now on, turn off 5 minutes later, turn on 10 minutes later.
     */
    public void setTimingSwitch(ArrayList<DutyTime> dutyTimeArrayList)
    {
        ArrayList<String>   powerStringList;

        Intent intent = new Intent();

        intent.setAction(BroadcastConsts.ACTION_TIMING_SWITCH);

        powerStringList = getPowerString(dutyTimeArrayList);

        Log.i(TAG, "power_string = " + powerStringList.toString());

        intent.putExtra(BroadcastConsts.POWER_STRING, powerStringList);

        mContext.sendBroadcast(intent);

        Toast.makeText(mContext, "power_string = " + powerStringList.toString(), Toast.LENGTH_SHORT).show();
    }

    /**
     *
     * @return
     */
    private ArrayList<String> getPowerString(ArrayList<DutyTime> dutyTimeList)
    {
        ArrayList<String>   powerArrayList;
        String  powerString = "";

        powerArrayList = new ArrayList<>();
        powerArrayList.clear();

        for (DutyTime dutyTime : dutyTimeList)
        {
            powerString = dutyTime.convertToPowerString();

            powerArrayList.add(powerString);
        }

        return  powerArrayList;
    }

    /**
     *  Delete All Timing Switch Settings.
     */
    public void deleteTimingSwitch()
    {
        Log.e(TAG, "send com.usw.set.time_delete broadcast");

        mContext.sendBroadcast(new Intent(BroadcastConsts.ACTION_TIME_DELETE));
    }

    /**
     *  Set brightness
     *  @param brightness
     */
    public void setBrightness(int brightness)
    {
        Intent intent = new Intent();

        intent.setAction(BroadcastConsts.ACTION_BRIGHTNESS_CONTROL);
        intent.putExtra(BroadcastConsts.KEY_BRIGHTNESS, brightness);

        mContext.sendBroadcast(intent);
    }

    /**
     *  Turn on Backlight
     *  @param on
     */
    public void setBkLight(boolean on)
    {
        Intent intent = new Intent();

        intent.setAction(BroadcastConsts.ACTION_BACKLIGHT_CONTROL);
        intent.putExtra(BroadcastConsts.KEY_BACKLIGHT_ENABLE, on);

        mContext.sendBroadcast(intent);
    }

    /**
     *  Do silent install
     *  @param apkFullPath
     */
    public void silentInstall(String apkFullPath)
    {
        Intent intent = new Intent();

        intent.setAction(BroadcastConsts.ACTION_INSTALL_APK);
        intent.putExtra(BroadcastConsts.APK_FULL_PATH, apkFullPath);

        mContext.sendBroadcast(intent);
    }

    /**
     *  Do Silent Uninstall
     *  @param packageName
     */
    public void silentUninstall(String packageName)
    {
        Intent intent = new Intent();

        intent.setAction(BroadcastConsts.ACTION_SILIENT_UNINSTALL);
        intent.putExtra(BroadcastConsts.APP_NAME, packageName);

        mContext.sendBroadcast(intent);
    }
}
