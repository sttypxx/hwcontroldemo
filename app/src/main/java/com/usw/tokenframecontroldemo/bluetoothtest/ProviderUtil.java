package com.usw.tokenframecontroldemo.bluetoothtest;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

/**
 *
 */
public class ProviderUtil {

    private static final String TAG = ProviderUtil.class.getSimpleName();

    public static final String  AUTHORITY_URL = "com.usw.tfhwcontrol.PROVIDER";
    public static final String  PATH_API            =   "api";
    public static final String  CONTENT_FULL_PATH   =   "content://" + AUTHORITY_URL + "/" + PATH_API;
    public static final Uri     CONTENT_URI         =   Uri.parse("content://" + AUTHORITY_URL + "/" + PATH_API);

    //  Get Device Id
    public static final String GET_DEVICE_ID                =   "getDeviceId";
    public static final String GET_PACKAGE_NAME             =   "getPackageName";
    public static final String GET_BLUETOOTH_MAC            =   "getBluetoothMac";
    public static final String GET_VER_INFO                 =   "getVerInfo";
    public static final String GET_BLUETOOTH_DEVICE_NAME    =   "getBluetoothDeviceName";
    public static final String IS_BLE_SUPPORT               =   "IsBleSupported";
    public static final String START_GATTSERVER             =   "StartGattServer";
    public static final String STOP_GATTSERVER              =   "StopGattServer";

    public static final String PARAMETER_RETURN             =   "result";

    /**
     *  Stop Gatt Server
     *  @param context
     *  @return
     */
    public static int StopGattServer(Context context)
    {
        String          method;
        Bundle          bundle;
        int             code, data;
        int             permissionResult;
        ContentResolver contentResolver;

        try
        {
            method  =   STOP_GATTSERVER;

            permissionResult = context.checkSelfPermission("com.usw.tfhwcontrol.provider.permission");
            if  (permissionResult == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Have com.usw.tfhwcontrol.provider.permission permission");
            }
            else {

                Log.i(TAG, "do not have com.usw.tfhwcontrol.provider.permission permission");
            }

            contentResolver = context.getContentResolver();
            if  (contentResolver == null) {

                Log.i(TAG, "can not get Content Resolver");

                return  0;
            }

            Log.i(TAG, "ContentPath = " + CONTENT_FULL_PATH);

            bundle  =   contentResolver.call(CONTENT_URI, method, null, null);

            if  (bundle != null) {

                code = bundle.getInt("code");
                data = bundle.getInt("data");

                Log.i(TAG, "StopGattServer(), code = " + code + ",data = " + data);

                return  (data != 0) ? 1 : 0;
            }
            else {

                Log.e(TAG, "ContentResolver.call() fails");

                return  0;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            return  0;
        }
    }

    /**
     *  Start Gatt Server
     *  @param context
     *  @return
     */
    public static int StartGattServer(Context context)
    {
        String          method;
        Bundle          bundle;
        int             code, data;
        int             permissionResult;
        ContentResolver contentResolver;

        try
        {
            method  =   START_GATTSERVER;

            permissionResult = context.checkSelfPermission("com.usw.tfhwcontrol.provider.permission");
            if  (permissionResult == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Have com.usw.tfhwcontrol.provider.permission permission");
            }
            else {

                Log.i(TAG, "do not have com.usw.tfhwcontrol.provider.permission permission");
            }

            contentResolver = context.getContentResolver();
            if  (contentResolver == null) {

                Log.i(TAG, "can not get Content Resolver");

                return  0;
            }

            Log.i(TAG, "ContentPath = " + CONTENT_FULL_PATH);

            bundle  =   contentResolver.call(CONTENT_URI, method, null, null);

            if  (bundle != null) {

                code = bundle.getInt("code");
                data = bundle.getInt("data");

                Log.i(TAG, "StartGattServer(), code = " + code + ",data = " + data);

                return  (data != 0) ? 1 : 0;
            }
            else {

                Log.e(TAG, "ContentResolver.call() fails");

                return  0;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            return  0;
        }
    }

    /**
     *
     * @param context
     * @return
     */
    public static boolean IsBleSupported(Context context)
    {
        String          method;
        Bundle          bundle;
        boolean         isBleSupported;
        int             code, data;
        int             permissionResult;
        ContentResolver contentResolver;

        try
        {
            method  =   IS_BLE_SUPPORT;

            permissionResult = context.checkSelfPermission("com.usw.tfhwcontrol.provider.permission");
            if  (permissionResult == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Have com.usw.tfhwcontrol.provider.permission permission");
            }
            else {

                Log.i(TAG, "do not have com.usw.tfhwcontrol.provider.permission permission");
            }

            contentResolver = context.getContentResolver();
            if  (contentResolver == null) {

                Log.i(TAG, "can not get Content Resolver");

                return  false;
            }

            Log.i(TAG, "ContentPath = " + CONTENT_FULL_PATH);

            bundle  =   contentResolver.call(CONTENT_URI, method, null, null);

            if  (bundle != null) {

                code = bundle.getInt("code");
                data = bundle.getInt("data");

                Log.i(TAG, "IsBleSupported(), code = " + code + ",data = " + data);

                return  (data != 0) ? true : false;
            }
            else {

                Log.e(TAG, "ContentResolver.call() fails");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  false;
    }

    /**
     *
     * @param context
     * @return
     */
    public static String getBluetoothMac(Context context)
    {
        String          method;
        String          paramReturn;
        Bundle          bundle;
        int             permissionResult;
        ContentResolver contentResolver;

        try
        {
            method  =   GET_BLUETOOTH_MAC;

            permissionResult = context.checkSelfPermission("com.usw.tfhwcontrol.provider.permission");
            if  (permissionResult == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Have com.usw.tfhwcontrol.provider.permission permission");
            }
            else {

                Log.i(TAG, "do not have com.usw.tfhwcontrol.provider.permission permission");
            }

            contentResolver = context.getContentResolver();
            if  (contentResolver == null) {

                Log.i(TAG, "can not get Content Resolver");

                return "";
            }

            Log.i(TAG, "ContentPath = " + CONTENT_FULL_PATH);

            bundle  =   contentResolver.call(CONTENT_URI, method, null, null);

            if  (bundle != null) {

                paramReturn = bundle.getString(PARAMETER_RETURN);

                Log.i(TAG, "getBluetoothMac(), paramReturn = " + paramReturn);

                return  paramReturn;
            }
            else {

                Log.e(TAG, "ContentResolver.call() fails");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  "";
    }

    /**
     *
     * @param context
     * @return
     */
    public static String getVerInfo(Context context)
    {
        String          method;
        String          paramReturn;
        Bundle          bundle;
        int             permissionResult;
        ContentResolver contentResolver;

        try
        {
            method  =   GET_VER_INFO;

            permissionResult = context.checkSelfPermission("com.usw.tfhwcontrol.provider.permission");
            if  (permissionResult == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Have com.usw.tfhwcontrol.provider.permission permission");
            }
            else {

                Log.i(TAG, "do not have com.usw.tfhwcontrol.provider.permission permission");
            }

            contentResolver = context.getContentResolver();
            if  (contentResolver == null) {

                Log.i(TAG, "can not get Content Resolver");

                return "";
            }

            Log.i(TAG, "ContentPath = " + CONTENT_FULL_PATH);

            bundle  =   contentResolver.call(CONTENT_URI, method, null, null);

            if  (bundle != null) {

                paramReturn = bundle.getString(PARAMETER_RETURN);

                Log.i(TAG, "getVerInfo(), paramReturn = " + paramReturn);

                return  paramReturn;
            }
            else {

                Log.e(TAG, "ContentResolver.call() fails");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  "";
    }

    /**
     *
     * @param context
     * @return
     */
    public static String getBluetoothDeviceName(Context context)
    {
        String          method;
        String          paramReturn;
        Bundle          bundle;
        int             permissionResult;
        ContentResolver contentResolver;

        try
        {
            method  =   GET_BLUETOOTH_DEVICE_NAME;

            permissionResult = context.checkSelfPermission("com.usw.tfhwcontrol.provider.permission");
            if  (permissionResult == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Have com.usw.tfhwcontrol.provider.permission permission");
            }
            else {

                Log.i(TAG, "do not have com.usw.tfhwcontrol.provider.permission permission");
            }

            contentResolver = context.getContentResolver();
            if  (contentResolver == null) {

                Log.i(TAG, "can not get Content Resolver");

                return "";
            }

            Log.i(TAG, "ContentPath = " + CONTENT_FULL_PATH);

            bundle  =   contentResolver.call(CONTENT_URI, method, null, null);

            if  (bundle != null) {

                paramReturn = bundle.getString(PARAMETER_RETURN);

                Log.i(TAG, "getBluetoothDeviceName(), paramReturn = " + paramReturn);

                return  paramReturn;
            }
            else {

                Log.e(TAG, "ContentResolver.call() fails");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  "";
    }
}
