package com.usw.tokenframecontroldemo.bluetoothtest;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.mylhyl.zxing.scanner.encode.QREncode;
import com.usw.tokenframecontroldemo.Application.MyApplication;
import com.usw.tokenframecontroldemo.MainActivity;
import com.usw.tokenframecontroldemo.R;
import com.usw.tokenframecontroldemo.bean.EventBean;
import com.usw.tokenframecontroldemo.receiver.BleReceiver;
import com.usw.tokenframecontroldemo.receiver.WifiConnectionReceiver;
import com.usw.tokenframecontroldemo.utils.BroadcastConsts;
import com.usw.tokenframecontroldemo.utils.ToastCustom;
import com.usw.tokenframecontroldemo.wifitest.WifiSupport;
import com.usw.wifiscan.bean.WifiConfig;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 *
 */
public class BluetoothTestActivity extends AppCompatActivity implements View.OnClickListener {

    private  static final String TAG = BluetoothTestActivity.class.getSimpleName();

    private static final int PERMISSIONS_ID = 1000;

    private     String              mBluetoothMac;
    private     String              mBluetoothDeviceName;
    private     ImageView           iv_bluetooth_qr;
    private     Button              btn_Clear;
    private     Button              btn_Quit;

    private     TextView            tv_bluetooth_mac;
    private     TextView            tv_bluetooth_deviceName;
    private     TextView            tv_server_msg;

    private BleReceiver             uiReceiver;
    private WifiConnectionReceiver  wifiConnectionReceiver;

    private String[] permissions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};

    public final static String ACTION_DATA_AVAILABLE =
            "com.usw.bluetooth.le.ACTION_DATA_AVAILABLE";

    public final static String EXTRA_DATA =
            "com.usw.bluetooth.le.EXTRA_DATA";

    private WifiConfig  wifiConfig = new WifiConfig();

    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(@NonNull Message msg) {

            switch(msg.what) {

                case 7071:
                    String  newWifiData = (String)msg.obj;
                    showText(newWifiData);

                    //  What can we do?
                    //  issue connect command to hardware control apk?
                    if  (parseReceivedMsg(newWifiData)) {

                        //  Received new Wifi Configuration
                        Message message = new Message();

                        message.what = 7072;
                        mHandler.sendMessage(message);
                    }
                    break;

                case 7072:

                    //  Display AlertDialog for user to choose
                    ShowChoiceForWifiConnection(wifiConfig);
                    break;
            }

            return false;
        }
    });

    /**
     *  onCreate
     *  @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_launcher);

        iv_bluetooth_qr = (ImageView)findViewById(R.id.iv_bluetooth_qr);

        btn_Clear = (Button)findViewById(R.id.btn_Clear);
        btn_Clear.setOnClickListener(this);

        btn_Quit  = (Button)findViewById(R.id.btn_Quit);
        btn_Quit.setOnClickListener(this);

        tv_server_msg = (TextView)findViewById(R.id.tv_bluetooth_server);

        tv_bluetooth_mac = (TextView)findViewById(R.id.tv_bluetooth_mac);

        tv_bluetooth_deviceName = (TextView)findViewById(R.id.tv_bluetooth_deviceName);

        handleBluetooth();

        //  Register Broadcast Receiver
        registerBroadCast();

        if  (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            //  Register Control Receiver
            registerControlReceiver();
        }

        //  register for event bean
        if  (!EventBus.getDefault().isRegistered(this)) { //加上判断

            EventBus.getDefault().register(this);
        }
    }

    /**
     *  displayBluetoothQRDisplay
     *  @param - bluetoothMac
     */
    private void displayBluetoothQRDisplay(String bluetoothMac) {

        String      deviceId = bluetoothMac;

        Log.i(TAG, "bluetooth MAC Address = " + bluetoothMac);

        if  (bluetoothMac == null) {

            Toast.makeText(BluetoothTestActivity.this, "bluetoothMac is null or empty", Toast.LENGTH_LONG).show();

            return;
        }

        tv_bluetooth_mac.setText(bluetoothMac);

        final Bitmap qrBitmap = new QREncode.Builder(BluetoothTestActivity.this)
                .setMargin(0)
                .setSize(160)
                .setContents(deviceId)  //  二维码内容
                .build().encodeAsBitmap();

        if  (iv_bluetooth_qr != null) {

            iv_bluetooth_qr.setImageBitmap(qrBitmap);
        }
    }

    /**
     *  Parse Received Message
     *  parseReceivedMsg
     *  @param msg
     */
    private boolean parseReceivedMsg(String msg)
    {
        JSONObject  topJsonObject;
        JSONObject  dataJsonObject;
        String      cmd;
        String      ssid;
        String      password;
        String      type;

        try
        {
            topJsonObject = new JSONObject(msg);

            if  (topJsonObject == null) {

                return  false;
            }

            cmd = topJsonObject.getString("cmd");

            if  (cmd.equals("Set"))     {

                dataJsonObject = topJsonObject.getJSONObject("data");
                if  (dataJsonObject == null)
                    return  false;

                ssid        =  dataJsonObject.getString("ssid");
                password    =  dataJsonObject.getString("password");
                type        =  dataJsonObject.getString("type");

                wifiConfig.setSsid(ssid);
                wifiConfig.setPassword(password);
                wifiConfig.setType(type);

                //

                return  true;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  false;
    }

    /**
     *  onStart
     */
    @Override
    protected void onStart() {

        super.onStart();

        initAll();
    }

    /**
     *  handleBluetooth
     */
    private void handleBluetooth()
    {
        Log.i(TAG, "Enter handleBluetooth");

        //  Bluetooth already opened, we can do our work
        mBluetoothMac = ProviderUtil.getBluetoothMac(BluetoothTestActivity.this);
        Log.i(TAG, "Local Bluetooth Mac is " + mBluetoothMac);

        mBluetoothDeviceName = ProviderUtil.getBluetoothDeviceName(BluetoothTestActivity.this);

        displayBluetoothQRDisplay(mBluetoothMac);
        tv_bluetooth_deviceName.setText(mBluetoothDeviceName);

        startServer();
    }

    /**
     *  registerBroadCast
     *  注册MainActivity的广播
     */
    private void registerBroadCast() {

        uiReceiver = new BleReceiver();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BleReceiver.ACTION_DATA_AVAILABLE);

        registerReceiver(uiReceiver, intentFilter);
    }

    /**
     *  registerControlReceiver
     *    --- Register Control Receiver
     */
    private void registerControlReceiver()
    {
        IntentFilter controlFilter = new IntentFilter();

        controlFilter.addAction(BroadcastConsts.ACTION_WIFI_CONNECT_STATUS);
        controlFilter.addAction(BroadcastConsts.ACTION_DATA_AVAILABLE);

        wifiConnectionReceiver = new WifiConnectionReceiver();

        registerReceiver(wifiConnectionReceiver, controlFilter);
    }

    /**
     *  unregisterControlReceiver
     *    --- Unregister Control Receiver
     */
    private void unregisterControlReceiver()
    {
        if  (wifiConnectionReceiver != null) {

            unregisterReceiver(wifiConnectionReceiver);
        }
    }

    /**
     *  unregisterBleReceiver
     */
    private void unregisterBleReceiver()
    {
        if  (uiReceiver != null) {

            unregisterReceiver(uiReceiver);
        }
    }

    /**
     *
     */
    @AfterPermissionGranted(PERMISSIONS_ID)
    private void methodRequiresTwoPermission() {

        initAll();
    }

    /**
     *  initAll
     */
    private void initAll() {

        if  (EasyPermissions.hasPermissions(this, permissions)) {

            //  Handle all bluetooth processing
            handleBluetooth();

        } else {

            EasyPermissions.requestPermissions(this, getString(R.string.permission_string), PERMISSIONS_ID, permissions);
        }
    }

    /**
     *  MainActivity EventBus message
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBeanMessage(EventBean eventBean) {

        switch(eventBean.getAction())
        {
            case EventBean.ACTION_RECEIVED_MESSAGE:

                //  get Wifi Config Data
                String  wifiData = eventBean.getMessage();
                Log.i(TAG, "Wifi Data: " + wifiData);

                Message msg =   Message.obtain();
                msg.what    =   7071;
                msg.obj     =   wifiData;

                mHandler.sendMessage(msg);
                break;

            default:
                break;
        }
    }

    /**
     *  onStop
     */
    @Override
    protected void onStop() {

        super.onStop();
    }

    /**
     *  onDestroy
     *  @param
     */
    @Override
    protected void onDestroy() {

        super.onDestroy();

        //  Stop GattServer
        stopServer();

        //  Unregister Ble Receiver
        unregisterBleReceiver();

        //  Unregister Control Receiver
        unregisterControlReceiver();

        //  unregister for event bean
        if  (EventBus.getDefault().isRegistered(this)) {  //加上判断

            EventBus.getDefault().unregister(this);
        }
    }

    /**
     *  Initialize the GATT server instance with the services/characteristics
     *  from the Time Profile.
     */
    private void startServer() {

        ProviderUtil.StartGattServer(MyApplication.getInstance());
    }

    /**
     *  Shut down the GATT server.
     */
    private void stopServer() {

        ProviderUtil.StopGattServer(MyApplication.getInstance());
    }

    private int flag = 0;

    /**
     *  Show Choice for connect to WiFi or not
     *  ShowChoiceForWifiConnection
     *  @param wifiConfig - Wifi Configuration
     */
    private void ShowChoiceForWifiConnection(WifiConfig wifiConfig)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(BluetoothTestActivity.this,
                android.R.style.Theme_Holo_Light_Dialog);

        builder.setTitle("Do you want to connect to WiFi " + wifiConfig.getSsid());
        builder.setIcon(android.R.drawable.ic_dialog_info);

        //  Specify the data to display
        final String[] actions = {"Ignore", "Send Command to Connect"};

        builder.setSingleChoiceItems(actions, 0, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                flag = which;
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                //  Do WiFi Connection
                doWiFiConnection(flag, wifiConfig);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    /**
     *  doWiFiConnection
     *  @param flag
     *  @param wifiConfig
     */
    private void doWiFiConnection(int flag, WifiConfig wifiConfig)
    {
        if  (flag == 0) {

            //  Ignore
            return;
        }
        else {

            //  Connect To Wifi Via Broadcast
            ConnectWiFiViaBroadcast(wifiConfig);
        }
    }

    /**
     *  ConnectWiFiViaBroadcast
     *  @param wifiConfig
     */
    private void ConnectWiFiViaBroadcast(WifiConfig wifiConfig) {

        Intent intent = new Intent(BroadcastConsts.ACTION_CONNECT_WIFI);
        intent.putExtra(BroadcastConsts.KEY_SSID,       wifiConfig.getSsid());
        intent.putExtra(BroadcastConsts.KEY_PASSWORD,   wifiConfig.getPassword());
        intent.putExtra(BroadcastConsts.KEY_TYPE,       wifiConfig.getType());

        sendBroadcast(intent);
    }

    /**
     *  showText
     *  @param msg
     */
    private void showText(final String msg) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                tv_server_msg.append(msg + "\r\n");
            }
        });
    }

    /**
     *  clearText
     */
    private void clearText() {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                tv_server_msg.setText("");
            }
        });
    }

    /**
     *
     *  @param v
     */
    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.btn_Clear:
                tv_server_msg.setText("");
                break;

            case R.id.btn_Quit:

                //  Quit this app
                finish();
                break;
        }
    }

    /**
     *
     */
    public class MainUiBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            try
            {
                String action = intent.getAction();

                Log.i(TAG, "onReceive, action = " + action);

                if  (action != null) {

                    switch (action) {

                        case ACTION_DATA_AVAILABLE:
                            //  get Wifi Config Data
                            String wifiData = intent.getStringExtra(EXTRA_DATA);
                            Log.i(TAG, "Wifi Data: " + wifiData);

                            Message msg = Message.obtain();
                            msg.what = 7071;
                            msg.obj = wifiData;
                            mHandler.sendMessage(msg);

                            break;

                        default:
                            break;
                    }
                }
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
    }
}