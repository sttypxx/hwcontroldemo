package com.usw.tokenframecontroldemo;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.usw.tokenframecontroldemo.Application.MyApplication;
import com.usw.tokenframecontroldemo.HwInterfaceManager.HwInterfaceManager;
import com.usw.tokenframecontroldemo.bean.DutyTime;
import com.usw.tokenframecontroldemo.bean.MessageEvent;
import com.usw.tokenframecontroldemo.bluetoothtest.BluetoothTestActivity;
import com.usw.tokenframecontroldemo.receiver.UpdateReceiver;
import com.usw.tokenframecontroldemo.utils.ProviderUtil;
import com.usw.tokenframecontroldemo.utils.RandomUtil;
import com.usw.tokenframecontroldemo.utils.ShellUtils;
import com.usw.tokenframecontroldemo.wifitest.WifiTestActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener {

    private static final String FrameWorkAppName = "com.usw.tokenframehwcontrol";
    private static final String TAG = MainActivity.class.getSimpleName();

    private UpdateReceiver appUpdateBroadcastReceiver;

    private Button      btn_LoadFramework;
    private Button      btn_reboot;
    private Button      btn_shutdown;
    private Button      btn_SetTimingSwitch;
    private Button      btn_DelTimingSwitch;
    private SeekBar     mSeekBar;
    private TextView    tv_brightnessvalue;
    private Button      btn_ScreenOnAndOff;
    private Button      btn_BkLightOnAndoff;
    private Button      btn_screenCap;

    private Button      btn_silentInstall;
    private Button      btn_silentUninstall;

    private Button      btn_getBluetoothMac;
    private Button      btn_getBluetoothDeviceName;
    private Button      btn_getVerInfo;

    private Button      btn_WifiTest;
    private Button      btn_BluetoothTest;

    private Button      btn_Exit;

    private static final int PERMISSIONS_REQUEST_CODE = 101;
    private static final int PERMISSIONS_ID = 1000;

    private String[] permissions = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();

        try {
            startHwFramework();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     */
    private void initUI() {

        btn_LoadFramework = (Button) findViewById(R.id.btn_LoadFramework);
        btn_reboot = (Button) findViewById(R.id.btn_reboot);
        btn_shutdown = (Button) findViewById(R.id.btn_shutdown);
        btn_ScreenOnAndOff = (Button) findViewById(R.id.btn_ScreenOnAndOff);
        btn_BkLightOnAndoff = (Button) findViewById(R.id.btn_BkLightOnAndoff);
        btn_SetTimingSwitch = (Button) findViewById(R.id.btn_SetTimingSwitch);
        btn_DelTimingSwitch = (Button) findViewById(R.id.btn_DelTimingSwitch);
        btn_screenCap = (Button) findViewById(R.id.btn_screenCap);

        btn_silentInstall = (Button) findViewById(R.id.btn_silentInstall);
        btn_silentUninstall = (Button) findViewById(R.id.btn_silentUninstall);

        btn_getBluetoothMac = (Button) findViewById(R.id.btn_getBluetoothMac);
        btn_getBluetoothDeviceName = (Button)findViewById(R.id.btn_getBluetoothDeviceName);
        btn_getVerInfo = (Button) findViewById(R.id.btn_getVerInfo);

        //----------------------------------------------
        //  Bluetooth and Wifi Test
        btn_WifiTest = (Button)findViewById(R.id.btn_WifiTest);
        btn_BluetoothTest   =   (Button)findViewById(R.id.btn_BluetoothTest);

        btn_Exit = (Button) findViewById(R.id.btn_Exit);

        mSeekBar = (SeekBar) findViewById(R.id.mSeekBar);
        tv_brightnessvalue = (TextView) findViewById(R.id.tv_brightnessvalue);

        btn_LoadFramework.setOnClickListener(this);

        //  reboot and shutdown
        btn_reboot.setOnClickListener(this);
        btn_shutdown.setOnClickListener(this);

        //  screen on/off
        btn_ScreenOnAndOff.setOnClickListener(this);
        btn_BkLightOnAndoff.setOnClickListener(this);

        //  time switch
        btn_SetTimingSwitch.setOnClickListener(this);
        btn_DelTimingSwitch.setOnClickListener(this);

        //  screen cap
        btn_screenCap.setOnClickListener(this);

        //  silent install/uninstall
        btn_silentInstall.setOnClickListener(this);
        btn_silentUninstall.setOnClickListener(this);

        //  get infos
        btn_getBluetoothMac.setOnClickListener(this);
        btn_getBluetoothDeviceName.setOnClickListener(this);
        btn_getVerInfo.setOnClickListener(this);

        //  wifi and bluetooth test
        btn_BluetoothTest.setOnClickListener(this);
        btn_WifiTest.setOnClickListener(this);

        //  exit
        btn_Exit.setOnClickListener(this);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                //  滑动中
                Log.i(TAG, "Seekbar is moving to " + progress);
                tv_brightnessvalue.setText("Brightness Value : " + progress);

                HwManager.getInstance(MainActivity.this).setBrightness(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                //  开始滑动
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                //  滑动结束
            }
        });
    }

    /**
     *  Get current System Brightness
     *  if returns -1, that is failure.
     *  @return
     */
    public static int getSystemBrightness(Context context) {

        if  (null != context) {

            try {

                return  Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);

            } catch (Settings.SettingNotFoundException e) {

                e.printStackTrace();
            }
        }

        return  -1;
    }

    private int flag = 0;

    /**
     *  onStart
     */
    @Override
    protected void onStart() {

        super.onStart();

        Log.i(TAG, "onStart");

        initAll();

        if  (!EventBus.getDefault().isRegistered(this)) { //加上判断

            EventBus.getDefault().register(this);
        }
    }

    /**
     *  onStop
     */
    @Override
    protected void onStop() {

        super.onStop();

        Log.i(TAG, "onStop");
    }

    /**
     *  onDestroy
     */
    @Override
    protected void onDestroy() {

        super.onDestroy();

        Log.i(TAG, "onDestroy");

        //  Unregister App Update Receiver
        unregisterAppUpdateReceiver();

        if  (EventBus.getDefault().isRegistered(this)) {  //加上判断

            EventBus.getDefault().unregister(this);
        }
    }

    /**
     *
     */
    @AfterPermissionGranted(PERMISSIONS_ID)
    private void methodRequiresTwoPermission() {

        initAll();
    }

    /**
     *  initAll
     */
    private void initAll() {

        if (EasyPermissions.hasPermissions(this, permissions)) {

            //  Register Control Receiver
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                //  Register App Update Receiver
                registerInstallAppBroadcastReceiver();
            }
        } else {

            EasyPermissions.requestPermissions(this, getString(R.string.permission_string), PERMISSIONS_ID, permissions);
        }
    }

    /**
     * @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_LoadFramework:

                //  Start Hw control Apk if needed
                startHwFrameworkIfNeeded();
                break;

            case R.id.btn_reboot:

                //  reboot
                HwManager.getInstance(MainActivity.this).reboot();
                break;

            case R.id.btn_shutdown:

                //  shutdown
                HwManager.getInstance(MainActivity.this).shutdown();
                break;

            case R.id.btn_SetTimingSwitch:

                //  do timing switch test
                doTimingSwitchTest();
                break;

            case R.id.btn_DelTimingSwitch:

                //  delete timing switch setting
                testClearTimingSwitchSetting();
                break;

            case R.id.btn_ScreenOnAndOff:

                //  test screen on and off
                testScreenOnAndOff();
                break;

            case R.id.btn_BkLightOnAndoff:

                //  test back light on and off
                testBkLightOnAndOff();
                break;

            case R.id.btn_screenCap:

                //  test screen cap
                testScreenCap();
                break;

            case R.id.btn_silentInstall:

                //  test silent install
                testSilentInstall();
                break;

            case R.id.btn_silentUninstall:

                //  test silent uninstall
                testSilentUninstall();
                break;

            case R.id.btn_getBluetoothMac:

                //  test get bluetooth mac
                getBluetoothMac();
                break;

            case R.id.btn_getBluetoothDeviceName:

                //  test to get bluetooth device name
                getBluetoothDeviceName();
                break;

            case R.id.btn_getVerInfo:

                //  test get verInfo via content provider
                getVerInfo();
                break;

            case R.id.btn_BluetoothTest:

                //  Do bluetooth test
                doBluetoothTest();
                break;

            case R.id.btn_WifiTest:

                //  Do wifi test
                doWifiTest();
                break;

            case R.id.btn_Exit:

                //  exit app
                finish();
                break;
        }
    }

    /**
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        super.onWindowFocusChanged(hasFocus);

        if  (hasFocus) {

            HideNavBarUtil.hideBottomUIMenu(getWindow().getDecorView());
        }
    }

    /**
     *  Register App Update broadcast receiver
     */
    private void registerInstallAppBroadcastReceiver() {

        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        intentFilter.addDataScheme("package");

        appUpdateBroadcastReceiver = new UpdateReceiver();

        registerReceiver(appUpdateBroadcastReceiver, intentFilter);
    }

    /**
     *  Unregister App Update Receiver
     */
    private void unregisterAppUpdateReceiver() {

        if (appUpdateBroadcastReceiver != null) {

            //  Unregister Control Receiver
            unregisterReceiver(appUpdateBroadcastReceiver);
        }
    }

    /**
     *  get Apps Dir
     *
     * @return
     */
    private String getAppsDir() {

        String strAppDir;

        File fileDir = getFilesDir();
        strAppDir = fileDir.getAbsolutePath();

        return  strAppDir;
    }

    /**
     *
     * @return
     */
//    private String getScreenShotFileName()
//    {
//        int year, month, day, hour, minute, second;
//        String      fileName;
//
//        Calendar c = Calendar.getInstance();
//
//        year    =   c.get(Calendar.YEAR);
//        month   =   (c.get(Calendar.MONTH) + 1);
//        day     =   c.get(Calendar.DAY_OF_MONTH);
//        hour    =   c.get(Calendar.HOUR_OF_DAY);
//        minute  =   c.get(Calendar.MINUTE);
//        second  =   c.get(Calendar.SECOND);
//
//        fileName = Integer.toString(year) + Integer.toString(month) + Integer.toString(day) + "_" +
//                Integer.toString(hour) + Integer.toString(minute) + Integer.toString(second);
//
//        return  fileName;
//    }

    /**
     *  Do timing switch test
     */
    private void doTimingSwitchTest() {

        ArrayList<DutyTime> dutyTimeArrayList;
        DutyTime dutyTime;
        int offHour, offMinute;
        int onHour, onMinute;

        dutyTimeArrayList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();

        //  1
        dutyTime = new DutyTime();

        calendar.add(Calendar.MINUTE, 3);
        offHour = calendar.get(Calendar.HOUR_OF_DAY);
        offMinute = calendar.get(Calendar.MINUTE);
        dutyTime.inputOffTime(offHour, offMinute);

        calendar.add(Calendar.MINUTE, 3);
        onHour = calendar.get(Calendar.HOUR_OF_DAY);
        onMinute = calendar.get(Calendar.MINUTE);
        dutyTime.inputOnTime(onHour, onMinute);

        dutyTime.setWeekMask(DutyTime.ALL_WEEKDAY_MASK);
        dutyTime.setEnable(true);

        dutyTimeArrayList.add(dutyTime);

        //  2
        dutyTime = new DutyTime();

        calendar.add(Calendar.MINUTE, 3);
        offHour = calendar.get(Calendar.HOUR_OF_DAY);
        offMinute = calendar.get(Calendar.MINUTE);
        dutyTime.inputOffTime(offHour, offMinute);

        calendar.add(Calendar.MINUTE, 3);
        onHour = calendar.get(Calendar.HOUR_OF_DAY);
        onMinute = calendar.get(Calendar.MINUTE);
        dutyTime.inputOnTime(onHour, onMinute);

        dutyTime.setWeekMask(DutyTime.ALL_WEEKDAY_MASK);
        dutyTime.setEnable(true);

        dutyTimeArrayList.add(dutyTime);

        HwManager.getInstance(MainActivity.this).setTimingSwitch(dutyTimeArrayList);
    }

    /**
     *  Clear Timing Switch Setting
     */
    private void testClearTimingSwitchSetting()
    {
        Toast.makeText(MainActivity.this, "Clear Timing Switch Setting", Toast.LENGTH_LONG).show();

        HwManager.getInstance(MainActivity.this).deleteTimingSwitch();
    }

    /**
     *  Start Hw Control Framework if needed
     */
    private void startHwFrameworkIfNeeded()
    {
        if  (isA133()) {

            //  No need to start, Launcher is the hw control apk
        }
        else {

            startHwFrameworkWay2();
        }
    }

    /**
     *
     */
    private void startHwFrameworkWay2()
    {
        Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage(FrameWorkAppName);
        LaunchIntent.setPackage(null); // 加上这句代码
        startActivity(LaunchIntent);
    }

    /**
     *  Start Hw Framework
     */
    private void startHwFramework()
    {
        int uid = AppUtils.getPackageUid(getApplicationContext(), FrameWorkAppName);

        if  (uid > 0) {

            boolean rstA = AppUtils.isAppRunning(getApplicationContext(), FrameWorkAppName);
            boolean rstB = AppUtils.isProcessRunning(getApplicationContext(), uid);

            if  (rstA || rstB) {

                //  The program with the specified package name is running
                Log.e(TAG, "Tokenframe Hw Framework is running");

            } else {

                //  The program with the specified package name is not running
                Log.e(TAG, "Tokenframe Hw Framework is not running, restart it");

                //  Start Not Hidden App
                AppUtils.doStartApplicationWithPackageName(getApplicationContext(), FrameWorkAppName);
            }
        }
    }

    private static final String TokenframePackageName = "com.usw.tokenframehwcontrol";
    private static final String TokenframeServiceName = "com.usw.tokenframehwcontrol.service.QHService";

    /**
     *  Turn on Screen
     */
    private void screenOn()
    {
        HwManager.getInstance(MainActivity.this).screenOn(MainActivity.this);
    }

    /**
     *  Turn Off Screen
     */
    private void screenOff()
    {
        HwManager.getInstance(MainActivity.this).screenOff(MainActivity.this);
    }

    /**
     *  Turn on Back Light
     */
    private void turnOnBkLight()
    {
        HwManager.getInstance(MainActivity.this).setBkLight(true);
    }

    /**
     *  Turn off Back Light
     */
    private void turnOffBkLight()
    {
        HwManager.getInstance(MainActivity.this).setBkLight(false);
    }

    /**
     *
     */
    private void testScreenOnAndOff()
    {
        Runnable    r = new Runnable() {
            @Override
            public void run() {

                try {
                    screenOn();

                    Thread.sleep(60 * 1000);

                    screenOff();

                    Thread.sleep(60 * 1000);

                    screenOn();
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        };

        new Thread(r).start();
    }

    /**
     *
     */
    private void testBkLightOnAndOff()
    {
        Runnable    r = new Runnable() {

            @Override
            public void run() {

                try {
                    Thread.sleep(20 * 1000);

                    turnOffBkLight();

                    Thread.sleep(20 * 1000);

                    turnOnBkLight();
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        };

        new Thread(r).start();
    }

    private static final String BASE_SCREEN_SHOT_PATH       =   "/storage/emulated/0/Pictures/";

    private static final String TOKEN_FRAME_PACKAGE_NAME    =   "com.tokenframe.tv";
    private static final String HW_CONTROL_PACKAGE_NAME     =   "com.usw.tokenframehwcontrol";
    private static final String A133_LAUNCHER_APP_NAME      =   "com.usw.ultralauncher";

    private static final String TOKEN_FRAME_LOW_VERSION     =   "Tokenframe_17-11-21_1453_v11.apk";
    private static final String TOKEN_FRAME_HIGHER_VERSION  =   "Tokenframe_21-12-21_1557_v17_prod.apk";

    private static final String USW_HW_CONTROL_APP_BND      =   "hwControlV3_BND_SMDT_3.1_202202191656_BND_release.apk";
    private static final String USW_HW_CONTROL_APP_SMDT     =   "hwControlV3_BND_SMDT_3.1_202202221537_smdt_release.apk";
    private static final String USW_HW_CONTROL_APP_YF3568   =   "hwControlV3_YF3568_3.1_202202241447_release_signed.apk";
    private static final String USW_ULTRA_LAUNCHER_APP      =   "TFLauncherV3_3.1_202202221053_release.apk";

    /**
     *  Test Screen Capture
     */
    private void testScreenCap()
    {
        String  basePath;
        String  imageFile;
        String  imageFileName;

        basePath = BASE_SCREEN_SHOT_PATH;

        imageFileName = RandomUtil.genRandomNum(6);
        imageFile = imageFileName + ".png";

        Toast.makeText(MainActivity.this, "screenshot, imagefile = " + basePath + imageFile, Toast.LENGTH_LONG).show();

        HwManager.getInstance(MainActivity.this).takeScreenshot(basePath, imageFile);
    }

    /**
     *
     * @param ins
     * @return
     * @throws IOException
     */
    private File inputstreamtofile(String fileName, InputStream ins) throws IOException {

        String  strAppDir;

        File    fileDir = getFilesDir();
        strAppDir = fileDir.getAbsolutePath();

//        File cacheDir = MyApplication.getApplicationInstance().getCacheDir();
//
//        Log.i(TAG, "cache dir path = " + cacheDir.getPath());

        Log.i(TAG, "file dir path = " + fileDir.getPath());
        File file = File.createTempFile(fileName, ".apk", fileDir);
        Log.d("TAG", "getAppDir() : " + strAppDir);

        Log.i(TAG, "file path = " + file.getAbsolutePath());

        OutputStream os = new FileOutputStream(file);
        int     bytesRead = 0;
        byte[]  buffer = new byte[8192];

        while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {

            os.write(buffer, 0, bytesRead);
        }

        os.close();
        ins.close();

        return  file;
    }

    //  apk 保存地址
    public static final String APK_DIRECTORY = Environment.getExternalStorageDirectory().getAbsolutePath();

    /**
     *
     * @param fileName
     * @param ins
     * @return
     * @throws IOException
     */
    private File InputStreamtOfileViaSdCard(String fileName, InputStream ins) throws IOException {

        String  strAppDir;

//        File fileDir = getFilesDir();
//        strAppDir = fileDir.getAbsolutePath();
//
//        File cacheDir = MyApplication.getApplicationInstance().getCacheDir();
//
//        Log.i(TAG, "cache dir path = " + cacheDir.getPath());
//
//        Log.i(TAG, "file dir path = " + fileDir.getPath());

        File apkDir = new File(APK_DIRECTORY);

        Log.d(TAG, "ApkDir : " + apkDir.getAbsolutePath());

        File file = File.createTempFile(fileName, ".apk", apkDir);

//        File file = File.createTempFile(fileName, ".apk", fileDir);
//        Log.d("TAG", "getAppDir() : " + strAppDir);

        Log.i(TAG, "file path = " + file.getAbsolutePath());

        OutputStream os = new FileOutputStream(file);
        int     bytesRead = 0;
        byte[]  buffer = new byte[8192];

        while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {

            os.write(buffer, 0, bytesRead);
        }

        os.close();
        ins.close();

        return  file;
    }

    /**
     *  Install Hw Control Apk
     */
    private void installApk(int flag)
    {
        String  apkFilePath;

        //  Copy hw control Apk for installation
        apkFilePath = CopyHwControlApkForInstallation(flag);

        //  chmod 666 apkfile
        ShellUtils.CommandResult chmod = ShellUtils.execCommand("chmod 666 " + apkFilePath, true);

//        apkFilePath = "/storage/A43B-F403/10DOT1-PAD/Tokenframe_17-11-21_1453_v11.apk";
        Log.i(TAG, "apkFilePath = " + apkFilePath);

        //  Do installation
        HwManager.getInstance(MainActivity.this).silentInstall(apkFilePath);
    }

    /**
     *
     * @return
     */
    private String getDeviceModel() {

        return  Build.MODEL;
    }

    /**
     *
     * @return
     */
    private String getDeviceProduct() {

        return  android.os.Build.PRODUCT;
    }

    /**
     *
     * @return
     */
    private boolean isA133()
    {
        String  deviceModel;

        deviceModel = getDeviceModel();

        if  (deviceModel.contains("CPF-01")) {

            Log.i(TAG, "A133 Board");

            return  true;
        }

        return  false;
    }

    /**
     *
     * @param flag
     */
    private void uninstallApk(int flag)
    {
        String  apkPackageToRemove = "";

        if  (flag == 0 || flag == 1) {

            apkPackageToRemove = TOKEN_FRAME_PACKAGE_NAME;
        }
        else {

            if  (isA133()) {

                apkPackageToRemove = A133_LAUNCHER_APP_NAME;
            }
            else {

                apkPackageToRemove = HW_CONTROL_PACKAGE_NAME;
            }
        }

        Toast.makeText(MainActivity.this, "Silent Uninstall, packageName = " + apkPackageToRemove, Toast.LENGTH_LONG).show();

        HwManager.getInstance(MainActivity.this).silentUninstall(apkPackageToRemove);
    }

    /**
     *
     */
    private String CopyHwControlApkForInstallation(int flag)
    {
        String  path = "";
        String  sourceFileName;

        if  (flag == 0) {

            //  Tokenframe lower version
            sourceFileName = TOKEN_FRAME_LOW_VERSION;
        }
        else if (flag == 1) {

            //  Tokenframe higher version
            sourceFileName = TOKEN_FRAME_HIGHER_VERSION;
        }
        else {

            //  hw Control Apk
            if  (HwInterfaceManager.getInstance().getBoardType().equals(HwInterfaceManager.BOARD_TYPE_A133)) {

                sourceFileName = USW_ULTRA_LAUNCHER_APP;
            }
            else if (HwInterfaceManager.getInstance().getBoardType().equals(HwInterfaceManager.BOARD_TYPE_BND)) {

                sourceFileName = USW_HW_CONTROL_APP_BND;
            }
            else if (HwInterfaceManager.getInstance().getBoardType().equals(HwInterfaceManager.BOARD_TYPE_SMDT)) {

                sourceFileName = USW_HW_CONTROL_APP_SMDT;
            }
            else if (HwInterfaceManager.getInstance().getBoardType().equals(HwInterfaceManager.BOARD_TYPE_SMDT_3399X)) {

                sourceFileName = USW_HW_CONTROL_APP_SMDT;
            }
            else if (HwInterfaceManager.getInstance().getBoardType().equals(HwInterfaceManager.BOARD_TYPE_YF3568)) {

                sourceFileName = USW_HW_CONTROL_APP_YF3568;
            }
            else {

                sourceFileName = USW_HW_CONTROL_APP_SMDT;
            }
        }

        Toast.makeText(MainActivity.this, "Install " + sourceFileName, Toast.LENGTH_LONG).show();

        try
        {
            InputStream is = MyApplication.getInstance().getAssets().open(sourceFileName);
//            path = inputstreamtofile("Tokenframe", is).getAbsolutePath();

            if  ((flag == 0) || (flag == 1)) {

                //  Tokenframe Main App
//                path = InputStreamtOfileViaSdCard("Tokenframe", is).getAbsolutePath();
                path = inputstreamtofile("Tokenframe", is).getAbsolutePath();
            }
            else {

                //  Hw Control App
//                path = InputStreamtOfileViaSdCard("hwControl", is).getAbsolutePath();
                path = inputstreamtofile("hwControl", is).getAbsolutePath();
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  path;
    }

    /**
     *  Show Choice for installation
     */
    private void ShowChoiceForInstall()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Holo_Light_Dialog);

        builder.setTitle("Select Apk to install");
        builder.setIcon(android.R.drawable.ic_dialog_info);

        //    Specify the data to display
        final String[] apks = {"Tokenframe Lower version", "Tokenframe Higher version", "Hw Control Apk"};

        builder.setSingleChoiceItems(apks, 0, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                flag = which;
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(MainActivity.this, "Selected Apks: " + apks[flag], Toast.LENGTH_LONG).show();

                dialog.dismiss();

                //  Install Hardware Control Apk
                installApk(flag);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    /**
     *  ShowChoiceForUninstall
     */
    private void ShowChoiceForUninstall()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Holo_Light_Dialog);

        builder.setTitle("Select Apk package to uninstall");
        builder.setIcon(android.R.drawable.ic_dialog_info);

        //    Specify the data to display
        final String[] apks = {"Tokenframe Lower version", "Tokenframe Higher version", "Hw Control Apk"};

        builder.setSingleChoiceItems(apks, 0, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                flag = which;
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(MainActivity.this, "Selected Apks: " + apks[flag], Toast.LENGTH_LONG).show();

                dialog.dismiss();

                //  uninstall
                uninstallApk(flag);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        builder.setCancelable(false);
        builder.show();
    }

    /**
     *  Test Silent Install
     */
    private void testSilentInstall()
    {
        ShowChoiceForInstall();
    }

    /**
     *  Test Silent Uninstall
     */
    private void testSilentUninstall()
    {
        ShowChoiceForUninstall();
    }

    /**
     *  get Bluetooth Real Mac Address
     */
    private void getBluetoothMac()
    {
        String  strBluetoothMac;

        strBluetoothMac = ProviderUtil.getBluetoothMac(MainActivity.this);

        Toast.makeText(this, "bluetooth Mac = " + strBluetoothMac, Toast.LENGTH_LONG).show();
    }

    /**
     *  get Blue tooth Device name
     */
    private void getBluetoothDeviceName()
    {
        String  strBluetoothDeviceName;

        strBluetoothDeviceName = ProviderUtil.getBluetoothDeviceName(MainActivity.this);

        Toast.makeText(this, "bluetooth Device Name = " + strBluetoothDeviceName, Toast.LENGTH_LONG).show();
    }

    /**
     *  get Tokenframe hw Control Apk version
     */
    private void getVerInfo()
    {
        String  strVerInfo;

        strVerInfo = ProviderUtil.getVerInfo(MainActivity.this);

        Toast.makeText(this, "VerInfo = " + strVerInfo, Toast.LENGTH_LONG).show();
    }

    /**
     *  Do Wifi Test
     *  It will launch another activity to do the test.
     */
    private void doWifiTest()
    {
        Intent intent = new Intent(MainActivity.this, WifiTestActivity.class);
        startActivity(intent);
    }

    /**
     *  Do Bluetooth Test
     *  It will launch another activity to do the test.
     */
    private void doBluetoothTest()
    {
        Intent intent = new Intent(MainActivity.this, BluetoothTestActivity.class);
        startActivity(intent);
    }

    /**
     *  MainActivity EventBus message
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventMessage(MessageEvent msgEvent) {

        Toast.makeText(MainActivity.this, msgEvent.getMessage(), Toast.LENGTH_LONG).show();
    }
}