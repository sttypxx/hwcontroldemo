package com.usw.tokenframecontroldemo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.usw.tokenframecontroldemo.MainActivity;

/**
 *
 */
public class BootReceiver extends BroadcastReceiver {

    private static final String TAG = BootReceiver.class.getSimpleName();

    private static final String BOOT_ACTION  = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i(TAG, "onReceive: ");

        if  (intent.getAction().equals(BOOT_ACTION)) {

            Intent intentNew = new Intent(context, MainActivity.class);

            intentNew.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentNew);
        }
    }
}
