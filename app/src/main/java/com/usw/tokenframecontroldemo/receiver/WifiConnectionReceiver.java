package com.usw.tokenframecontroldemo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.usw.tokenframecontroldemo.utils.BroadcastConsts;

public class WifiConnectionReceiver extends BroadcastReceiver
{
    private static final String TAG = BootReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i(TAG, "onReceive: ");

        if  (intent.getAction().equals(BroadcastConsts.ACTION_WIFI_CONNECT_STATUS)) {

            String  ssid = intent.getStringExtra(BroadcastConsts.STATUS_KEY_SSID);
            int     result = intent.getIntExtra(BroadcastConsts.STATUS_KEY_RESULT, 0);

            Log.i(TAG, "Wifi Connect Status, ssid = " + ssid + ",result = " + result);
        }
        else if (intent.getAction().equals(BroadcastConsts.ACTION_DATA_AVAILABLE)) {

            String  wifiConfig = intent.getStringExtra(BroadcastConsts.EXTRA_DATA);

            Log.i(TAG, "Wifi Configuration Data, wifiConfig = " + wifiConfig);
        }
    }
}
