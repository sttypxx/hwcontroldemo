package com.usw.tokenframecontroldemo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.usw.tokenframecontroldemo.bean.EventBean;

import org.greenrobot.eventbus.EventBus;

public class BleReceiver extends BroadcastReceiver
{
    private static final String TAG = BleReceiver.class.getSimpleName();

    public final static String ACTION_DATA_AVAILABLE =
            "com.usw.bluetooth.le.ACTION_DATA_AVAILABLE";

    public final static String EXTRA_DATA =
            "com.usw.bluetooth.le.EXTRA_DATA";

    /**
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        try
        {
            String action = intent.getAction();

            Log.i(TAG, "onReceive, action = " + action);

            if  (action != null) {

                switch (action) {

                    case ACTION_DATA_AVAILABLE:
                        //  get Wifi Config Data
                        String wifiData = intent.getStringExtra(EXTRA_DATA);
                        Log.i(TAG, "Wifi Data: " + wifiData);

                        EventBean   eventBean = new EventBean();
                        eventBean.setAction(EventBean.ACTION_RECEIVED_MESSAGE);
                        eventBean.setMessage(wifiData);

                        EventBus.getDefault().post(eventBean);
                        break;

                    default:
                        break;
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
