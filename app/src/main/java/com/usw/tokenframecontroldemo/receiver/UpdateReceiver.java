package com.usw.tokenframecontroldemo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.usw.tokenframecontroldemo.bean.MessageEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

/**
 *
 */
public class UpdateReceiver extends BroadcastReceiver {

    private static final String TAG = UpdateReceiver.class.getSimpleName();

    private static final String TOKEN_FRAME_MAIN_APP    =   "com.tokenframe.tv";

    private boolean             isUpdate = false;
    private Context             mContext;

    /**
     *
     *  @param context
     *  @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        mContext = context;

        try
        {
            String action = intent.getAction();

            //  app包名
            String  packageName = intent.getData().getSchemeSpecificPart();

            if  (action != null) {

                switch (action) {

                    //  应用更换广播
                    case "android.intent.action.PACKAGE_REPLACED":

                        isUpdate = isAppWeMonitored(packageName);

                        if  (isUpdate) {

                            reportPackageReplaced(packageName);
                        }
                        break;

                    //  接收安装广播
                    case "android.intent.action.PACKAGE_ADDED":

                        isUpdate = isAppWeMonitored(packageName);

                        if  (isUpdate) {

                            reportPackageAdded(packageName);
                        }
                        break;

                    //  接收卸载广播
                    case "android.intent.action.PACKAGE_REMOVED":

                        isUpdate = isAppWeMonitored(packageName);

                        if  (isUpdate) {

                            reportPackageRemoved(packageName);
                        }
                        break;

                    default:
                        break;
                }
            }
        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            if  (isUpdate) {

                isUpdate = false;
            }
        }
    }

    /**
     *  Judge whether it is the app monitored
     *  @param appName
     *  @return
     */
    private boolean isAppWeMonitored(String appName)
    {
        String  appToBeMonitored;

        appToBeMonitored = TOKEN_FRAME_MAIN_APP;

        if  (appName.equals(appToBeMonitored))
        {
            return  true;
        }

        return  false;
    }

    /**
     *  Report Package Replaced
     *  @param packageName
     */
    private void reportPackageReplaced(String packageName)
    {
        String  message;

        message = "Package " + packageName + " Replaced";

        EventBus.getDefault().post(new MessageEvent(message));

        Log.i(TAG, "The replacement of " + packageName + " Apk file is detected");
    }

    /**
     *  Report Package Added
     *  @param packageName
     */
    private void reportPackageAdded(String packageName)
    {
        String  message;

        message = "Package " + packageName + " Added";

        EventBus.getDefault().post(new MessageEvent(message));

        Log.i(TAG, "The addition of " + packageName + " Apk file is detected");
    }

    /**
     *  Report Package Removed
     *  @param packageName
     */
    private void reportPackageRemoved(String packageName)
    {
        String  message;

        message = "Package " + packageName + " Removed";

        EventBus.getDefault().post(new MessageEvent(message));

        Log.i(TAG, "The removal of " + packageName + " app is detected");
    }
}
