package com.usw.tokenframecontroldemo.wifitest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.usw.tokenframecontroldemo.Application.MyApplication;
import com.usw.tokenframecontroldemo.R;
import com.usw.tokenframecontroldemo.utils.ToastCustom;

import java.util.ArrayList;
import java.util.List;

import static android.net.wifi.WifiManager.EXTRA_RESULTS_UPDATED;

/**
 *
 */
public class SettingNetworkFragment extends Fragment
{
    private static final String TAG = SettingNetworkFragment.class.getSimpleName();

    private Switch          mWifiSwitchBtn;
    private ImageView       mLoadingView;
    private RecyclerView    mWifiRecyclerView;
    private TextView        mWifiOpenStateView;

    private WifiManager     mWifiManager;

    private Animation       mLoadingAnim;

    List<ScanResult>        scanResults = new ArrayList<>();

    private WifiScanReceiver mReceiver;

    private WifiSettingListAdapter wifiSettingListAdapter;

    private int wifiStartScanAction =   1011;
    private boolean isConnecting    =   false;

    /**
     *
     */
    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(@NonNull Message msg) {

            if  (wifiStartScanAction == msg.what) {

                if  (mWifiManager != null) {

                    connectionInfo = mWifiManager.getConnectionInfo();
                    if  (connectionInfo != null) {

                        Log.i(TAG, "WiFi currently connected is " + connectionInfo.toString());
                    }
                }

                //  not Connecting
                if  (!isConnecting) {

                    if  (mWifiManager != null) {

                        if  (mWifiManager.isWifiEnabled()) {

                            boolean scan = mWifiManager.startScan();
                            Log.i(TAG, scan ? "wifi  scan is start" : "wifi scan is not start");

                        } else {

                            if  (mLoadingView != null) {

                                if (mLoadingView.getAnimation() != null) {
                                    mLoadingView.clearAnimation();
                                }
                            }
                        }
                    }

                    if  (mLoadingView != null) {

                        if  (mLoadingAnim != null) {

                            mLoadingView.startAnimation(mLoadingAnim);
                        }
                    }
                }

                if  (mHandler.hasMessages(wifiStartScanAction)) {

                    mHandler.removeMessages(wifiStartScanAction);
                }

                mHandler.sendEmptyMessageDelayed(wifiStartScanAction, 10 * 1000);
            }

            return  false;
        }
    });

    private WifiInfo    connectionInfo;
    private Context     mContext;

    private WifiSettingListAdapter.ItemClickListener itemClickListener = new WifiSettingListAdapter.ItemClickListener() {

        @Override
        public void onClick(WifiInfoBean wifiInfoBean) {

            WifiConnectDialogFragment     wifiConnectDialogFragment = new WifiConnectDialogFragment(mWifiManager, wifiInfoBean);

            FragmentManager fragmentManager = getFragmentManager();

            if  (fragmentManager != null) {

                wifiConnectDialogFragment.show(fragmentManager, "wifi_connect");

            } else {

                Log.e(TAG, "wifi setting fragment manager is null");
            }
        }
    };

    /**
     *
     *  @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mContext = getContext();
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View    view = inflater.inflate(R.layout.fragment_setting_network, container, false);

        mWifiSwitchBtn      =   view.findViewById(R.id.id_wifi_switch);
        mWifiRecyclerView   =   view.findViewById(R.id.id_wifi_setting_list);
        mLoadingView        =   view.findViewById(R.id.id_wifi_loading);
        mWifiOpenStateView  =   view.findViewById(R.id.id_wifi_open_state);

        return  view;
    }

    /**
     *
     */
    @Override
    public void onStart() {

        super.onStart();

        mReceiver = new WifiScanReceiver();
        mContext.registerReceiver(mReceiver, mReceiver.getIntentFilter());

        mLoadingAnim = AnimationUtils.loadAnimation(getContext(), R.anim.loading_anim);
        mLoadingView.setVisibility(View.INVISIBLE);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mWifiRecyclerView.setLayoutManager(linearLayoutManager);

        mWifiManager = (WifiManager) MyApplication.getInstance().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if  (mWifiManager != null) {

            connectionInfo = mWifiManager.getConnectionInfo();
            if  (connectionInfo != null) {

                Log.i(TAG, "WiFi currently connected is " + connectionInfo.toString());
            }
        }

        wifiSettingListAdapter = new WifiSettingListAdapter(getContext());
        wifiSettingListAdapter.setItemClickListener(itemClickListener);
        wifiSettingListAdapter.setScanResults(scanResults, connectionInfo);

        mWifiRecyclerView.setAdapter(wifiSettingListAdapter);

        if  (mWifiManager != null) {

            mWifiSwitchBtn.setChecked(mWifiManager.isWifiEnabled());
            mWifiOpenStateView.setText(mWifiManager.isWifiEnabled() ?
                    getContext().getResources().getString(R.string.wifi_is_open) :
                    getContext().getResources().getString(R.string.wifi_is_close));

            if  (mWifiManager.isWifiEnabled()) {

                mHandler.removeMessages(wifiStartScanAction);
                mHandler.sendEmptyMessageDelayed(wifiStartScanAction, 0);
            }
        }

        mWifiSwitchBtn.setOnCheckedChangeListener(switchListener);
    }

    /**
     *  onStop
     */
    @Override
    public void onStop() {

        super.onStop();

        mHandler.removeMessages(wifiStartScanAction);

        mContext.unregisterReceiver(mReceiver);
    }

    /**
     *  onDestroy
     */
    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    /**
     *
     */
    private CompoundButton.OnCheckedChangeListener switchListener = new CompoundButton.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            if  (isChecked) {

                if  (!mWifiManager.isWifiEnabled()) {

                    ToastCustom.syncOnesShowToast(mContext, mContext.getResources().getString(R.string.wifi_is_open));

                    mWifiOpenStateView.setText(mContext.getResources().getString(R.string.wifi_is_open));
                    mWifiManager.setWifiEnabled(true);
                }

                mHandler.removeMessages(wifiStartScanAction);
                mHandler.sendEmptyMessageDelayed(wifiStartScanAction, 0);

            } else {

                wifiSettingListAdapter.setScanResults(null, connectionInfo);

                ToastCustom.syncOnesShowToast(mContext, mContext.getResources().getString(R.string.wifi_is_close));
                mWifiOpenStateView.setText("WIFI OFF");

                if  (mWifiManager.isWifiEnabled()) {

                    mWifiManager.setWifiEnabled(isChecked);

                } else {

                    Log.i(TAG, "wifi is not enabled");
                }

                if  (mLoadingView.getAnimation() != null) {

                    mLoadingView.clearAnimation();
                }

                mLoadingView.setVisibility(View.INVISIBLE);

                mHandler.removeMessages(wifiStartScanAction);
            }
        }
    };

    /**
     *  WifiScanReceiver
     */
    public class WifiScanReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if  (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {

                boolean scanSuccessful = intent.getBooleanExtra(EXTRA_RESULTS_UPDATED, false);

                scanResults = mWifiManager.getScanResults();

                Log.i(TAG, "scan results " + scanResults.toString());

                if  (scanResults != null) {

                    wifiSettingListAdapter.setScanResults(scanResults, connectionInfo);
                }

                if  (mLoadingView.getAnimation() != null) {

                    mLoadingView.clearAnimation();
                    mLoadingView.setVisibility(View.INVISIBLE);
                }
            }
        }

        /**
         *
         * @return
         */
        public IntentFilter getIntentFilter() {

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);

            return  intentFilter;
        }
    }
}
