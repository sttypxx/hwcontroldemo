package com.usw.tokenframecontroldemo.wifitest;

public class WifiInfoBean implements Comparable<WifiInfoBean> {

    private String  ssid;
    private String  bssid;
    private String  capabilities;   //  Judge whether there have password or not
    private boolean connectState;
    private String  signalQuality;  //  Signal Quality

    //  ssid
    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    //  connectState
    public boolean isConnectState() {
        return connectState;
    }

    public void setConnectState(boolean connectState) {
        this.connectState = connectState;
    }

    //  signalQuality
    public String getSignalQuality() {
        return signalQuality;
    }

    public void setSignalQuality(String signalQuality) {
        this.signalQuality = signalQuality;
    }

    //  bssid
    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    //  capabilities
    public String getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(String capabilities) {
        this.capabilities = capabilities;
    }

    @Override
    public int compareTo(WifiInfoBean o) {
        return o.connectState ? 1 : -1;
    }
}
