package com.usw.tokenframecontroldemo.wifitest;

import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.usw.tokenframecontroldemo.R;
import com.usw.tokenframecontroldemo.receiver.WifiConnectionReceiver;
import com.usw.tokenframecontroldemo.utils.BroadcastConsts;

public class WifiTestActivity extends AppCompatActivity {

    private static final String TAG = WifiTestActivity.class.getSimpleName();

    private static int handlerWifiConfigurationChanged           =   11136;

//    private WifiService     mWifiService;
    private WifiConnectionReceiver wifiConnectionReceiver;

    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(@NonNull Message msg) {

            if  (msg.what == handlerWifiConfigurationChanged) {

                Log.i(TAG, "Notify the wifi configuration");

//                notifyMonitorService();
            }

            return  false;
        }
    });

    /**
     *  onCreate
     *  @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_test);

        if  (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            //  Register Control Receiver
            registerControlReceiver();
        }

        loadMixFragment();
    }

    /**
     *  load Mix Fragment for photo and video slide show.
     */
    private void loadMixFragment() {

        try
        {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

            SettingNetworkFragment settingNetworkFragment = new SettingNetworkFragment();

            fragmentTransaction.replace(R.id.id_root_frame_layout, settingNetworkFragment,
                                        SettingNetworkFragment.class.getSimpleName());

            fragmentTransaction.commitNowAllowingStateLoss();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     *
     */
    @Override
    protected void onStart() {

        super.onStart();

        //  register for receiving messages
//        if  (!EventBus.getDefault().isRegistered(this)) { //加上判断
//
//            EventBus.getDefault().register(this);
//        }
    }

    /**
     *  onDestroy
     */
    @Override
    protected void onDestroy() {

        super.onDestroy();

        //  unregister for receiving messages.
//        if  (EventBus.getDefault().isRegistered(this)) {  //加上判断
//
//            EventBus.getDefault().unregister(this);
//        }

        unregisterControlReceiver();
    }

    /**
     *  Register Control Receiver
     */
    private void registerControlReceiver()
    {
        IntentFilter controlFilter = new IntentFilter();

        controlFilter.addAction(BroadcastConsts.ACTION_WIFI_CONNECT_STATUS);

        wifiConnectionReceiver = new WifiConnectionReceiver();

        registerReceiver(wifiConnectionReceiver, controlFilter);
    }

    /**
     *  Unregister Control Receiver
     */
    private void unregisterControlReceiver()
    {
        if  (wifiConnectionReceiver != null) {

            unregisterReceiver(wifiConnectionReceiver);
        }
    }

    /**
     *
     * @param bean
     */
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void eventMessage(EventBean bean) {
//
//        if  (bean.getAction() == EventBean.NOTIFY_WIFI_CONFIGURATION_CHANGES) {
//
//            Message message = Message.obtain();
//            message.what =  handlerWifiConfigurationChanged;
//            message.obj  =  "";
//
//            mHandler.sendMessage(message);
//        }
//    }
}

