package com.usw.tokenframecontroldemo.wifitest;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.usw.tokenframecontroldemo.R;
import com.usw.tokenframecontroldemo.utils.BroadcastConsts;
import com.usw.tokenframecontroldemo.utils.ToastCustom;
import com.usw.wifiscan.bean.WifiConfig;

import java.util.List;

/**
 *
 */
public class WifiConnectDialogFragment extends DialogFragment
        implements View.OnClickListener
{
    private static final String TAG = WifiConnectDialogFragment.class.getSimpleName();

    private TextView    mSsidView;
    private EditText    mPasswordEditView;
    private TextView    mCancelBtn;
    private TextView    mDisconnectBtn;
    private TextView    mHeadInfoView;

    private WifiManager mWifiManger;
    private WifiInfoBean wifiInfoBean;

    private Handler     mHandler = new Handler(Looper.getMainLooper());
    private Context     mContext;

    private IntentFilter    filter;
//    private WifiConnectionReceiver wifiConnectionReceiver;
    private WifiConfig  wifiConfigParam;

    /**
     *
     *  @param wifiManager
     *  @param wifiInfoBean
     */
    public WifiConnectDialogFragment(WifiManager wifiManager, WifiInfoBean wifiInfoBean) {

        this.mWifiManger = wifiManager;
        this.wifiInfoBean = null;
        this.wifiInfoBean = wifiInfoBean;

        List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();

        for (WifiConfiguration wifiConfiguration : configuredNetworks) {

            Log.i(TAG, "wfi configuration message is " + wifiConfiguration);
        }

        wifiConfigParam = new WifiConfig();
    }

    /**
     *  onCreate
     *  @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mContext = getContext();

//        wifiConnectionReceiver = new WifiConnectionReceiver(this);
//        filter   =  getConnectionStatusFilter();
//
//        getActivity().registerReceiver(wifiConnectionReceiver, filter);
    }

    /**
     *  onCreateView
     *  @param inflater
     *  @param container
     *  @param savedInstanceState
     *  @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_wifi_connext, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        mSsidView           =   view.findViewById(R.id.id_wifi_connect_ssid_view);
        mPasswordEditView   =   view.findViewById(R.id.id_wifi_password_edit_view);
        mCancelBtn          =   view.findViewById(R.id.id_wifi_cancel_btn);
        mDisconnectBtn      =   view.findViewById(R.id.id_wifi_disconnect_btn);
        mHeadInfoView       =   view.findViewById(R.id.id_wifi_head_info_view);

        mCancelBtn.setOnClickListener(this);
        mDisconnectBtn.setOnClickListener(this);

        mSsidView.setText(String.format("SSID: %s", wifiInfoBean.getSsid()));

        if  (wifiInfoBean.isConnectState()) {

            mHeadInfoView.setText(R.string.wifi_connect_head_info);
            mPasswordEditView.setEnabled(false);
            mPasswordEditView.setHint("");
            mDisconnectBtn.setTag("disconnect");
            mDisconnectBtn.setText(R.string.btn_wifi_disconnect);

        } else {

            mHeadInfoView.setText(R.string.wifi_input_password_head_info);
            mPasswordEditView.setEnabled(true);
            mDisconnectBtn.setTag("connect");
            mPasswordEditView.setHint(R.string.wifi_password);
            mDisconnectBtn.setText(R.string.btn_wifi_connect);
        }

        if  (WifiSupport.getWifiCipher(wifiInfoBean.getCapabilities()) == WifiSupport.WifiCipherType.WIFICIPHER_NOPASS) {

            mPasswordEditView.setEnabled(false);
        }

        return  view;
    }

    /**
     *  onStart
     */
    @Override
    public void onStart() {

        super.onStart();

        try
        {
            Dialog dialog = getDialog();

            if (dialog != null) {

                DisplayMetrics dm = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                dialog.getWindow().setLayout((int) (dm.widthPixels * 0.6), (int) (dm.heightPixels * 0.5));

                dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_dialog));
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    /**
     *  onStop
     */
    @Override
    public void onStop() {

        super.onStop();

        wifiInfoBean    =   null;
        mWifiManger     =   null;
    }

    /**
     *  onDestroy
     */
    @Override
    public void onDestroy() {

        super.onDestroy();

//        //  Unregister Wifi Connection Receiver
//        getActivity().unregisterReceiver(wifiConnectionReceiver);
    }

    /**
     *  onClick
     *  @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.id_wifi_connect_btn:
                break;

            case R.id.id_wifi_disconnect_btn:

                if  ("connect".equals(v.getTag())) {

                    if (getSecurity(wifiInfoBean) == WIFI_NO_PASS) {

                        connectNoPasswordWifiViaBroadcast();
                    } else {

                        String password = mPasswordEditView.getText().toString();
                        connectPasswordWifiViaBroadcast(password);
                    }
                } else if ("disconnect".equals(v.getTag())) {

                    disconnectWifi();
                }
                break;

            case R.id.id_wifi_cancel_btn:

                dismiss();
                break;

            default:
                break;
        }
    }

    /**
     *  disconnectWifi
     *
     */
    @SuppressLint("CheckResult")
    private void disconnectWifi() {

        if  (mWifiManger != null) {

            WifiInfo connectionInfo = mWifiManger.getConnectionInfo();
            int networkId = connectionInfo.getNetworkId();
            mWifiManger.disableNetwork(networkId);
            mWifiManger.disconnect();
        }

        ToastCustom.showToast(mContext, getResources().getString(R.string.wifi_connnect_to_disconnect));

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, 2000);
    }

    /**
     *  connectPasswordWifiViaBroadcast
     *  @param password
     */
    private void connectPasswordWifiViaBroadcast(final String password) {

        Log.i(TAG, "Enter connectPasswordWifiViaBroadcast");

        wifiConfigParam.setSsid(wifiInfoBean.getSsid());
        wifiConfigParam.setPassword(password);
        wifiConfigParam.setType(WifiSupport.getWifiCipherString(wifiInfoBean.getCapabilities()));

        ToastCustom.showToast(mContext, getResources().getString(R.string.wifi_is_connecting));

        Intent intent = new Intent(BroadcastConsts.ACTION_CONNECT_WIFI);
        intent.putExtra(BroadcastConsts.KEY_SSID,       wifiConfigParam.getSsid());
        intent.putExtra(BroadcastConsts.KEY_PASSWORD,   wifiConfigParam.getPassword());
        intent.putExtra(BroadcastConsts.KEY_TYPE,       wifiConfigParam.getType());

        getActivity().sendBroadcast(intent);

        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                    dismiss();
            }
        }, 2000);
    }

    /**
     *  connectNoPasswordWifiViaBroadcast
     *  Connect To No password Wifi via broadcast to the hardware control APK
     */
    private void connectNoPasswordWifiViaBroadcast() {

        Log.i(TAG, "Enter connectNoPasswordWifiViaBroadcast");

        wifiConfigParam.setSsid(wifiInfoBean.getSsid());
        wifiConfigParam.setPassword("");
        wifiConfigParam.setType(WifiSupport.getWifiCipherString(wifiInfoBean.getCapabilities()));

        ToastCustom.showToast(mContext, getResources().getString(R.string.wifi_is_connecting));

        Intent intent = new Intent(BroadcastConsts.ACTION_CONNECT_WIFI);
        intent.putExtra(BroadcastConsts.KEY_SSID,       wifiConfigParam.getSsid());
        intent.putExtra(BroadcastConsts.KEY_PASSWORD,   wifiConfigParam.getPassword());
        intent.putExtra(BroadcastConsts.KEY_TYPE,       wifiConfigParam.getType());

        getActivity().sendBroadcast(intent);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, 2000);
    }

    public static final int WIFI_NO_PASS = 0;

    private static final int WIFI_WEP = 1;
    private static final int WIFI_PSK = 2;
    private static final int WIFI_EAP = 3;

    /**
     *  Judge Wifi Encryption type
     *
     *  @param result ScanResult
     *  @return 0
     */
    public static int getSecurity(WifiInfoBean result) {

        if  (null != result && null != result.getCapabilities()) {

            if  (result.getCapabilities().contains("WEP")) {

                return  WIFI_WEP;

            } else if (result.getCapabilities().contains("PSK")) {

                return  WIFI_PSK;

            } else if (result.getCapabilities().contains("EAP")) {

                return  WIFI_EAP;
            }
        }

        return  WIFI_NO_PASS;
    }

    /**
     *  getConnectionStatusFilter
     *  @return
     */
    public IntentFilter getConnectionStatusFilter()
    {
        IntentFilter    filter = new IntentFilter();

        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

        return  filter;
    }

    /**
     *  isConnected
     *  @param info
     *  @return
     */
    private boolean isConnected(WifiInfo info)
    {
        if  (info != null && !TextUtils.isEmpty(info.getSSID())) {

            String  targetSSID = wifiConfigParam.getSsid();

            if  (!TextUtils.isEmpty(targetSSID) && info.getSSID().contains(targetSSID)) {

                Log.i(TAG, "5 connected success,  wifi  = " + info.getSSID());

                return  true;
            }
        }

        return  false;
    }

    /**
     *
     * @param info
     */
//    @Override
//    public void connectedWifiCallback(WifiInfo info) {
//
//        final boolean isConnect = isConnected(info);
//
//        if  (isConnect) {
//
////            ConfigManager.getInstance().setWifiConfig(wifiConfigParam);
////
////            Log.i(TAG, "WifiConfigParam = " + wifiConfigParam.toString());
////
////            //  Post an Event
////            EventBean   eventBean = new EventBean();
////            eventBean.setAction(EventBean.NOTIFY_WIFI_CONFIGURATION_CHANGES);
////            eventBean.setMsg("");
////
////            EventBus.getDefault().post(eventBean);
//        }
//        else
//        {
//            Log.i(TAG, "Wifi cannot be connected ");
//        }
//    }
}
