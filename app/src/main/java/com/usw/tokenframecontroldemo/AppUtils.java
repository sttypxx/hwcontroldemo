package com.usw.tokenframecontroldemo;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import android.util.Log;

import java.util.Iterator;
import java.util.List;


/**
 *
 *
 */
public class AppUtils {

    private static final String TAG = AppUtils.class.getSimpleName();

    /**
     *
     * @param context
     * @param className
     * @return
     */
    public static boolean isForeground(Context context, String className) {

        if  (context == null || TextUtils.isEmpty(className)) {

            return false;
        }

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1);
        if (list != null && list.size() > 0) {

            ComponentName cpn = list.get(0).topActivity;
            if  (className.equals(cpn.getClassName())) {

                return true;
            }
        }

        return false;

    }

    /**
     * Judge whether the app is running or not.
     *
     *
     * @param context
     * @param packageName
     * @return
     */
    public static boolean isAppRunning(Context context, String packageName) {

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(100);
        if  (list.size() <= 0) {

            return false;
        }

        for (ActivityManager.RunningTaskInfo info : list) {

            if  (info.baseActivity.getPackageName().equals(packageName)) {
                return  true;
            }
        }

        return  false;
    }

    /**
     * 获取已安装应用的 uid，-1 表示未安装此应用或程序异常
     * @param context
     * @param packageName
     * @return
     */
    public static int getPackageUid(Context context, String packageName) {

        try
        {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(packageName, 0);

            if  (applicationInfo != null) {

                Log.e("TAG", String.valueOf(applicationInfo.uid));

                return  applicationInfo.uid;
            }

        } catch (Exception e) {

            return  -1;
        }

        return  -1;
    }

    /**
     *
     * @param context
     * @param uid
     * @return true
     */
    public static boolean isProcessRunning(Context context, int uid) {

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> runningServiceInfos = am.getRunningServices(200);

        if  (runningServiceInfos.size() > 0) {

            for (ActivityManager.RunningServiceInfo appProcess : runningServiceInfos) {

                if  (uid == appProcess.uid) {

                    return true;
                }
            }
        }

        return  false;
    }

    /**
     *
     * @param context
     * @param packagename
     */
    public static void doStartApplicationWithPackageName(Context context, String packagename) {

        PackageInfo     packageinfo = null;

        try
        {
            packageinfo = context.getPackageManager().getPackageInfo(packagename, 0);

        } catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();
        }

        if  (packageinfo == null) {

            Log.e(TAG, "packageinfo is null");

            return;
        }

        Intent resolveIntent = new Intent(Intent.ACTION_MAIN, null);
        resolveIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        resolveIntent.setPackage(packageinfo.packageName);

        List<ResolveInfo> resolveinfoList = context.getPackageManager()
                .queryIntentActivities(resolveIntent, 0);

        Iterator<ResolveInfo> iterator = resolveinfoList.iterator();
        if  (iterator.hasNext()) {

            ResolveInfo resolveinfo = iterator.next();

            if  (resolveinfo != null) {

                //  packagename = 参数packname
                String packageName = resolveinfo.activityInfo.packageName;

                //  这个就是我们要找的该APP的LAUNCHER的Activity[组织形式：packagename.mainActivityname]
                String className = resolveinfo.activityInfo.name;

                //  LAUNCHER Intent
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);

                //  设置ComponentName参数1:packagename参数2:MainActivity路径
                ComponentName cn = new ComponentName(packageName, className);

                intent.setComponent(cn);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

                Log.e(TAG, "Start MainActivity of " + packageName);
            }
        }
        else
        {
            Log.e(TAG, "resolveinfoList.iterator() no next ");
        }
    }

    /**
     *
     * @param context
     */
    public static void startPackageWithBroadcast(Context context)
    {
        Intent  intent = new Intent("com.usw.request_boot");
        context.sendBroadcast(intent);
    }

    /**
     *
     * @param context
     * @param cls
     */
    @SuppressLint("WrongConstant")
    public static void restartApp(Context context, Class<?> cls) {

        Intent intent = new Intent(context,cls);
        PendingIntent restartIntent = PendingIntent.getActivity(
                context, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);

        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if  (mgr != null) {

            //  1秒钟后重启应用
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10,
                    restartIntent);
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }
}
