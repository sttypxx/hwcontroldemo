package com.usw.tokenframecontroldemo.Application;

import android.app.Application;
import android.content.Context;

import com.usw.tokenframecontroldemo.HwInterfaceManager.HwInterfaceManager;

import xcrash.XCrash;

public class MyApplication extends Application {

    private static MyApplication    mInstance;

    /**
     *
     * @param base
     */
    @Override
    protected void attachBaseContext(Context base) {

        super.attachBaseContext(base);

        XCrash.init(this);

        mInstance = this;

        HwInterfaceManager.getAndSetupInstance(mInstance);
    }

    /**
     *
     */
    public static Application getInstance() {

        return  mInstance;
    }
}
