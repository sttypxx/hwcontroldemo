package com.usw.tokenframecontroldemo.bean;

public class DutyTime {

    public static final int DAY = 1440;          //  1440 minutes per day = 24 * 60
    public static final int HOUR = 60;           //  60 minutes per hour
    public static final int WEEK = DAY * 7;      //  10080 = 1440 * 7 minutes per week

    //  Day of Week Mask
    public static final int     MONDAY_MASK     =  0x1;
    public static final int     TUESDAY_MASK    =  0x2;
    public static final int     WEDNESDAY_MASK  =  0x4;
    public static final int     THURSDAY_MASK   =  0x8;
    public static final int     FRIDAY_MASK     =  0x10;
    public static final int     SATURDAY_MASK   =  0x20;
    public static final int     SUNDAY_MASK     =  0x40;
    public static final int     ALL_WEEKDAY_MASK=  0x7F;

    private int     onHour,     onMinute;
    private int     offHour,    offMinute;

    private int     weekMask;
    private boolean enable;

    /**
     *
     * @param onHour
     * @param onMinute
     */
    public void inputOnTime(int onHour, int onMinute)
    {
        this.onHour     =   onHour;
        this.onMinute   =   onMinute;
    }

    /**
     *
     * @param offHour
     * @param offMinute
     */
    public void inputOffTime(int offHour, int offMinute)
    {
        this.offHour     =   offHour;
        this.offMinute   =   offMinute;
    }

    //  Week Mask
    public int getWeekMask() {
        return  weekMask;
    }

    public void setWeekMask(int weekMask) {
        this.weekMask = weekMask;
    }

    //  Enable
    public boolean isEnable() {
        return  enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    /**
     *
     * @return
     */
    public String convertToPowerString()
    {
        String  hourString = "";
        String  dayString = "";
        String  enableString = "";
        String  powerString = "";
        boolean first;

        //  Hour String
        hourString = String.format("%02d:%02d-%02d:%02d", offHour, offMinute, onHour, onMinute);

        //  Day String
        first     = true;
        dayString = "";

        //  Sunday
        if  ((weekMask & SUNDAY_MASK) != 0) {
            if  (first) {
                dayString += "7";
                first = false;
            }
            else {
                dayString += ",7";
            }
        }

        //  Monday
        if  ((weekMask & MONDAY_MASK) != 0) {
            if  (first) {
                dayString += "1";
                first = false;
            }
            else {
                dayString += ",1";
            }
        }

        //  Tuesday
        if  ((weekMask & TUESDAY_MASK) != 0) {
            if  (first) {
                dayString += "2";
                first = false;
            }
            else {
                dayString += ",2";
            }
        }

        //  Wednesday
        if  ((weekMask & WEDNESDAY_MASK) != 0) {
            if  (first) {
                dayString += "3";
                first = false;
            }
            else {
                dayString += ",3";
            }
        }

        //  Thursday
        if  ((weekMask & THURSDAY_MASK) != 0) {
            if  (first) {
                dayString += "4";
                first = false;
            }
            else {
                dayString += ",4";
            }
        }

        //  Friday
        if  ((weekMask & FRIDAY_MASK) != 0) {
            if  (first) {
                dayString += "5";
                first = false;
            }
            else {
                dayString += ",5";
            }
        }

        //  Saturday
        if  ((weekMask & SATURDAY_MASK) != 0) {
            if  (first) {
                dayString += "6";
                first = false;
            }
            else {
                dayString += ",6";
            }
        }

        //  enableString
        if  (enable) {
            enableString = "true";
        }
        else {
            enableString = "false";
        }

        powerString = (hourString + "|" + dayString + "|" + enableString);

        return  powerString;
    }
}
