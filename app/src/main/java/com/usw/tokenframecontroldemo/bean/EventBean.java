package com.usw.tokenframecontroldemo.bean;

public class EventBean {

    public static final int ACTION_RECEIVED_MESSAGE = 110000;


    private int     action;
    private String  message;

    //  action
    public int getAction() {

        return  action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    //  message
    public String getMessage() {

        return  message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
