package com.usw.wifiscan.delegate;

import android.content.Context;
import android.net.wifi.ScanResult;

import java.util.List;

/**
 *
 */
public interface WifiDelegate {

    void wifiScan(Context context);

    List<ScanResult> getWifiScanResult(Context context);

    int getCurrentIndex();

    void stopScan();
}
