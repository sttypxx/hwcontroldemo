package com.usw.wifiscan.Utils;

import android.app.Service;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;

import com.usw.wifiscan.bean.WifiConfig;
import com.usw.wifiscan.broadcast.WifiBroadcastReceiver;
import com.usw.wifiscan.delegate.WifiDelegate;
import com.usw.wifiscan.impl.WifiServiceDelegateImpl;
import com.usw.wifiscan.listener.ScanResultListener;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 *
 */
public class YueWifiHelper {

    private WeakReference<Service>      mContext;
    private WifiDelegate                delegate;
    private WifiBroadcastReceiver       receiver;
    private IntentFilter                filter;
    private ScanResultListener          listener;
    private WifiConfig                  wifiConfig;

    /**
     *
     * @param context
     * @param listener
     */
    public YueWifiHelper(Service context, ScanResultListener listener) {

        init(context, listener);
    }

    /**
     *  Configure Wifi
     *  @param wifiConfig
     */
    public void ConfigWifi(WifiConfig wifiConfig)
    {
        this.wifiConfig = wifiConfig;

        WifiUtil.getInstance().ConfigWifi(wifiConfig);
    }

    /**
     *  1. initialize the delegate
     *  2. register the broadcast
     */
    public void init(Service context, ScanResultListener listener) {

        mContext =  new WeakReference<>(context);
        delegate =  new WifiServiceDelegateImpl();
        receiver =  new WifiBroadcastReceiver(delegate, listener);

        filter   =  WifiUtil.getInstance().initFilter();

        if  (mContext != null && mContext.get() != null) {

            mContext.get().registerReceiver(receiver, filter);
        }

        this.listener = listener;
    }

    /**
     *  3. Start Wifi Scan
     */
    public void startScan() {

        if  (delegate != null) {

            delegate.wifiScan(mContext.get());
        }
    }

    /**
     *  4. Handle the scan, if successfully get the target wifi, stop scanning; else, continue to scan until SCAN_TIMES
     *  5. When getting the target wifi, stop scan and start to connect
     */

    public void filterAndConnectTargetWifi(List<ScanResult> list, WifiConfig wifiConfig, boolean isLastTime) {

        WifiUtil.getInstance().filterAndConnectTargetWifi(mContext.get(), delegate, list, wifiConfig, isLastTime, listener);
    }

    /**
     *  6. Make sure that the target wifi is connected
     */
    public boolean isConnected(WifiInfo info) {

        return  WifiUtil.getInstance().ensureConnectSuc(mContext.get(), info);
    }

    /**
     *  7. Stop Scan
     */
    public void stop() {

        delegate.stopScan();
    }

    /**
     *  8. Unregister
     */
    public void destroy() {

        mContext.get().unregisterReceiver(receiver);
    }
}
