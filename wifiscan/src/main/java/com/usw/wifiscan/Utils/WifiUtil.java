package com.usw.wifiscan.Utils;

import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;

import com.usw.wifiscan.bean.SimpleWifiBean;
import com.usw.wifiscan.bean.WifiCipherType;
import com.usw.wifiscan.bean.WifiConfig;
import com.usw.wifiscan.delegate.WifiDelegate;
import com.usw.wifiscan.listener.ScanResultListener;

import java.util.List;

/**
 *
 */

public class WifiUtil {

    private static final String TAG = WifiUtil.class.getSimpleName();

//    public static final String  SP_NAME  =   "WIFI_INFO_SAVE";
//    public static final String  SSID_KEY =   "TARGET_SSID_KEY";
//    public static final String  MI_WIFI  =   "MI_WIFI";
//
//    public static final int     MI_REQUEST_CODE     =   0x111;
//    public static final int     BEST_RECORD_TIME    =   400;

    private WifiConfig          wifiConfig;

    private static WifiUtil     instance = null;

    /**
     *
     */
    private WifiUtil()
    {

    }

    /**
     *
     * @return
     */
    public static WifiUtil getInstance() {

        if  (instance == null) {

            instance = new WifiUtil();
        }

        return  instance;
    }

    /**
     *
     *  @return
     */
    public IntentFilter initFilter() {

        IntentFilter    filter = new IntentFilter();

        filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

        return  filter;
    }

    /**
     *
     *  @param wifiConfig
     */
    public void ConfigWifi(WifiConfig wifiConfig)
    {
        this.wifiConfig = wifiConfig;
    }

    /**
     *
     *  @param context
     *  @param info
     *  @return
     */
    public boolean ensureConnectSuc(Context context, WifiInfo info) {

        if  (info != null && !TextUtils.isEmpty(info.getSSID())) {

            String  targetSSID = wifiConfig.getSsid();

            if  (!TextUtils.isEmpty(targetSSID) && info.getSSID().contains(targetSSID)) {

                Log.i(TAG, "5 connected success,  wifi  = " + info.getSSID());

                return  true;
            }
        }

        Log.i(TAG, "5 connected fail ");

        return  false;
    }

    /**
     *  Filter wifi and connect to target wifi
     *  This will be called repeated until get the target wifi or reach the upper limit of trying
     *
     */
    public void filterAndConnectTargetWifi(Context context,         WifiDelegate delegate,
                                            List<ScanResult> list,  WifiConfig wifiConfig,
                                            boolean isLastTime,     ScanResultListener listener) {

        String  targetWifiName;

        targetWifiName = wifiConfig.getSsid();

        SimpleWifiBean wifiBean = FilterTargetWifi(context, delegate, list, targetWifiName);
        if  (wifiBean != null) {

            Log.i(TAG, "3 filter, success");

            connectTargetWifi(context, wifiConfig, wifiBean);
        }

        if  (isLastTime && (wifiBean == null)) {

            Log.i(TAG, "3 filter, fail");

            if  (listener != null) {

                listener.filterFailure();
            }
        }
    }

    /**
     *  Connect to target Wifi
     *
     *  @param context
     *  @param wifiBean
     */
    public void connectTargetWifi(Context context, WifiConfig wifiConfig, SimpleWifiBean wifiBean) {

        String  capabilities = wifiBean.getCapabilities();

        if  (getWifiCipher(capabilities) == WifiCipherType.WIFICIPHER_NOPASS) {

            //  No password
            WifiConfiguration tempConfig = isExists(wifiBean.getWifiName(), context);
            if  (tempConfig == null) {

                Log.i(TAG, "4 connect, tempConfig = null");

                WifiConfiguration existsConfig = createWifiConfig(wifiBean.getWifiName(), null,
                        WifiCipherType.WIFICIPHER_NOPASS);

                addNetWork(existsConfig, context);

            } else {

                Log.i(TAG, "4 connect, tempConfig.SSID = " + tempConfig.SSID);

                addNetWork(tempConfig, context);
            }

        } else {

            //  Need password to connect
            Log.i(TAG, "Need password to connect");

            connectPasswordWifi(context, wifiBean, wifiConfig);
        }
    }

    /**
     *  connect to Wifi with password
     *
     *  @param context
     *  @param wifiBean
     *  @param wifiConfig
     */
    private void connectPasswordWifi(Context context, SimpleWifiBean wifiBean, WifiConfig wifiConfig) {

        WifiConfiguration   tempConfig;

        tempConfig = isExists(wifiBean.getWifiName(), context);
        if  (tempConfig == null) {

            Log.i(TAG, "connectPasswordWifi, tempConfig = null");

            WifiConfiguration existWifiConfig =
                    createWifiConfig(wifiBean.getWifiName(), wifiConfig.getPassword(),
                            WifiUtil.getInstance().getWifiCipher(wifiBean.getCapabilities()));

            addNetWork(existWifiConfig, context);

        } else {

            Log.i(TAG, "Already have this config : " + tempConfig.SSID);

            addNetWork(tempConfig, context);
        }
    }

    /**
     *  Filter Target Wifi
     *  @param context -
     *  @param delegate
     *  @param list
     *  @param targetWifiName
     */
    public SimpleWifiBean FilterTargetWifi(Context context, WifiDelegate delegate,
                                           List<ScanResult> list, String targetWifiName) {

        SimpleWifiBean  simpleWifiBean = null;

        for  (ScanResult result : list) {

            if  (result.SSID.contains(targetWifiName)) {

                delegate.stopScan();

                simpleWifiBean = new SimpleWifiBean();

                simpleWifiBean.setWifiName(result.SSID);
                simpleWifiBean.setCapabilities(result.capabilities);
                simpleWifiBean.setLevel(getLevel(result.level) + "");
            }
        }

        return  simpleWifiBean;
    }

    /**
     *  Get Signal Level
     *  @param level
     *  @return
     */
    public int getLevel(int level) {

        if  (Math.abs(level) < 50) {

            return  1;
        } else if (Math.abs(level) < 75) {

            return  2;
        } else if (Math.abs(level) < 90) {

            return  3;
        } else {

            return  4;
        }
    }

    /**
     *  Judge whether wifi configuration with the SSID name exists or not.
     *
     *  @param SSID
     *  @param context
     *  @return
     */
    public WifiConfiguration isExists(String SSID, Context context) {

        WifiManager wifimanager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> existingConfigs = wifimanager.getConfiguredNetworks();

        for (WifiConfiguration existingConfig : existingConfigs) {

            if  (existingConfig.SSID.contains(SSID)) {

                return  existingConfig;
            }
        }

        return  null;
    }

    /**
     *  Judge the encryption type of wifi hotspot
     *  @param s
     */
    public WifiCipherType getWifiCipher(String s) {

        if  (s.isEmpty()) {

            return  WifiCipherType.WIFICIPHER_INVALID;

        } else if (s.contains("WEP")) {

            return  WifiCipherType.WIFICIPHER_WEP;

        } else if (s.contains("WPA") || s.contains("WPA2") || s.contains("WPS")) {

            return  WifiCipherType.WIFICIPHER_WPA;

        } else {

            return  WifiCipherType.WIFICIPHER_NOPASS;
        }
    }

    /**
     *  Create WifiConfiguration with SSID, password and encryption type
     *  @param SSID
     *  @param password
     *  @param type
     *  @return
     */
    public WifiConfiguration createWifiConfig(String SSID, String password, WifiCipherType type) {

        WifiConfiguration config = new WifiConfiguration();

        config.allowedAuthAlgorithms.clear();
        config.allowedGroupCiphers.clear();
        config.allowedKeyManagement.clear();
        config.allowedPairwiseCiphers.clear();
        config.allowedProtocols.clear();
        config.SSID = "\"" + SSID + "\"";

        if  (type == WifiCipherType.WIFICIPHER_NOPASS) {

            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        }

        if  (type == WifiCipherType.WIFICIPHER_WEP) {

            config.preSharedKey = "\"" + password + "\"";
            config.hiddenSSID = true;
            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            config.wepTxKeyIndex = 0;
        }

        if  (type == WifiCipherType.WIFICIPHER_WPA) {

            config.preSharedKey = "\"" + password + "\"";
            config.hiddenSSID = true;
            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            config.status = WifiConfiguration.Status.ENABLED;
        }

        return  config;
    }

    /**
     *  connect to wifi
     *  @param config
     *  @param context
     */
    public boolean addNetWork(WifiConfiguration config, Context context) {

        WifiManager wifimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        WifiInfo    wifiinfo = wifimanager.getConnectionInfo();

        if  (null != wifiinfo) {

            wifimanager.disableNetwork(wifiinfo.getNetworkId());
        }

        boolean result = false;

        if  (config.networkId > 0) {

            result = wifimanager.enableNetwork(config.networkId, true);

            wifimanager.updateNetwork(config);

        } else {

            int i = wifimanager.addNetwork(config);
            result = false;

            if  (i > 0) {

                wifimanager.saveConfiguration();
                return  wifimanager.enableNetwork(i, true);
            }
        }

        return  result;
    }
}
