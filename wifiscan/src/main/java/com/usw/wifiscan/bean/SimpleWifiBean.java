package com.usw.wifiscan.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *  Describe a Wifi hot pot
 */
public class SimpleWifiBean implements Parcelable {

    private String  wifiName;
    private String  password;
    private String  level;
//    private String state;         //  connected, connecting, disconnected
    private String  capabilities;   //  encryption type

    /**
     *
     */
    public SimpleWifiBean() {

    }

    /**
     *
     * @param in
     */
    protected SimpleWifiBean(Parcel in) {

        wifiName        =   in.readString();
        setPassword(in.readString());
        level           =   in.readString();
        capabilities    =   in.readString();
    }

    public static final Creator<SimpleWifiBean> CREATOR = new Creator<SimpleWifiBean>() {

        @Override
        public SimpleWifiBean createFromParcel(Parcel in) {
            return new SimpleWifiBean(in);
        }

        @Override
        public SimpleWifiBean[] newArray(int size) {
            return new SimpleWifiBean[size];
        }
    };

    //  wifiName
    public String getWifiName() {
        return wifiName;
    }

    public void setWifiName(String wifiName) {
        this.wifiName = wifiName;
    }

    //  password
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //  level
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    //  capabilities
    public String getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(String capabilities) {
        this.capabilities = capabilities;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     *
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(wifiName);
        dest.writeString(password);
        dest.writeString(level);
//        dest.writeString(state);
        dest.writeString(capabilities);
    }
}
