package com.usw.wifiscan.bean;

import java.io.Serializable;

/**
 * Wifi Configuration
 */
public class WifiConfig implements Serializable {

    private String  ssid;
    private String  password;
    private String  type;

    /**
     *
     */
    public WifiConfig()
    {
        ssid     = "";
        password = "";
        setType("");
    }

    //  ssid
    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    //  password
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //  type
    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    /***
     *
     * @return
     */
    @Override
    public String toString()
    {
        return  ("(ssid = " + ssid + ",password = " + password + ",type = " + type + ")");
    }
}
