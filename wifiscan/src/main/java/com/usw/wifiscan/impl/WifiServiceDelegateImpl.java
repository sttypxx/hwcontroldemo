package com.usw.wifiscan.impl;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.usw.wifiscan.delegate.WifiDelegate;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class WifiServiceDelegateImpl implements WifiDelegate {

    private static final String TAG = WifiServiceDelegateImpl.class.getSimpleName();

    private final int DELAY_TIME = 2000;    //  delay time
    private final int TOTAL_TIMES = 10;     //  maximum scan count

    private ScheduledExecutorService pool;
    private volatile int index;             //  current scan index

    /**
     *  Do Wifi Scan
     *  @param context
     */
    @Override
    public void wifiScan(final Context context) {

        index = 0;

        if  (pool != null) {

            pool.shutdown();

            pool = null;
        }

        pool = Executors.newScheduledThreadPool(1);

        final WifiManager wifimanager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        pool.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {

                if (index < TOTAL_TIMES) {

                    wifimanager.startScan();

                    Log.i(TAG, "1 :  wifimanager.startScan()" + " index = " + index);

                    index++;
                }
            }
        }, 0, DELAY_TIME, TimeUnit.MILLISECONDS);
    }

    /**
     *
     * @param context
     * @return
     */
    @Override
    public List<ScanResult> getWifiScanResult(Context context) {

        return  ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getScanResults();
    }

    /**
     *
     * @return
     */
    @Override
    public int getCurrentIndex() {
        return index;
    }

    /**
     *
     */
    @Override
    public void stopScan() {

        if  (pool != null) {

            pool.shutdown();
        }
    }
}